def tuple_ex():
    # declare tuple
    t1 = ("GOOG", 100, 490.1)
    t2 = "GOOG", 100, 490.1
    print(t1, t2)


def dict_ex():
    # create dictionary from two lists
    keys = list("abcd")
    values = [i for i in range(0, 4)]
    d1 = dict(zip(keys, values))
    print(d1)
    # {'a': 0, 'b': 1, 'c': 2, 'd': 3}

    # add or modify values
    d1["a"] = 7
    d1["e"] = 4
    print(d1)
    # {'a': 7, 'b': 1, 'c': 2, 'd': 3, 'e': 4}

    # del values
    del d1["e"]
    print(d1)
    # {'a': 7, 'b': 1, 'c': 2, 'd': 3}

    # get all keys
    print(list(d1))
    # ['a', 'b', 'c', 'd']

    # iterate keys
    for k in d1:
        print(k, "=", d1[k])

    # return currently available keys
    k1 = d1.keys()
    print(k1)
    del d1["a"]
    print(k1)

    # return currently available keys and values
    items = d1.items()
    for k, v in d1.items():
        print(k, "=", v)

    # list of tuples to dict
    print(items)
    # dict_items([('b', 1), ('c', 2), ('d', 3)])
    d2 = dict(items)
    print(d2)
    # {'b': 1, 'c': 2, 'd': 3}

    # return value if key exists, otherwise return None
    key = "b"
    x = d1[key] if key in d1 else None
    y = d1.get(key, None)
    print(x, y)
    # 1 1

    # tuple keys
    keys = list(zip(list("abc"), list("abc")))
    vals = [i for i in range(0, 3)]
    d1 = dict(zip(keys, vals))
    print(dict(zip(keys, vals)), d1["a", "a"])


def set_usage():
    # declaration
    s1 = {i for i in range(0, 3)}
    print(s1)
    # {0, 1, 2}

    # find value
    print(1 in s1)
    # True

    # duplicate elimination
    s2 = ["test" for i in range(0, 3)]
    print(s2, set(s2))
    # ['test', 'test', 'test'] {'test'}

    # add item
    s1.add("a")
    print(s1)
    s1.remove("a")
    print(s1)

    # union/intersection/difference
    s1 = set("test")
    s2 = set("rekt")
    print(f"Union: {s1 | s2}")
    print(f"Intersection: {s1 & s2}")
    print(f"Difference: {s1 - s2}")
    print(f"Symmetric Difference: {s1 ^ s2}")


def type_cast():
    c1, i1, f1 = ("a", "1", "1.7")
    maths = int(i1) / float(f1)

    # print 2 decimals after dot
    print(f"{maths:0.2f}")
    # 0.59


if __name__ == "__main__":
    # tuple_ex()
    # dict_ex()
    set_usage()
    # type_cast()
