import io
import json
import os
import shutil
from typing import NamedTuple
from subprocess import run

import requests
import typer
import yaml
from typer import colors, secho

app = typer.Typer()


class Data(NamedTuple):
    """Represents an employee."""
    id: str
    name: str
    symbol: str
    current_price: float


class Coin():

    def __init__(self):
        file = "coin.yml"
        with open(file, "r") as stream:
            try:
                data = yaml.safe_load(stream)
                # print(json.dumps(data, indent=1))
            except yaml.YAMLError as e:
                print(e)
        self.d = data

    def jprint(self):
        # create a formatted string of the Python JSON object
        text = json.dumps(self.d, sort_keys=True, indent=4)
        secho(text, fg=colors.BLUE)

    def select(self):
        # print(self.d.get("id"))
        d = self.d
        print(d)
        try:
            print(d['id'])
        except KeyError:
            print("Cannot find id")
        # o = Data(id=d.get("id"), name=d.get("name"), symbol=d.get("symbol"), current_price=d.get(""))
        # print(o)


@app.command()
def test():
    c = Coin()
    c.select()
    # for i in c.select():
    #     print(i)


@app.command()
def update():
    url = "https://api.coingecko.com/api/v3/coins"
    response = requests.get(url)
    print(response.status_code)
    result = response.json()
    # self.jprint(result)
    file = "coin.yml"
    with io.open(file, 'w', encoding='utf8') as outfile:
        yaml.dump(result[0], outfile, default_flow_style=False, allow_unicode=True)
    secho(f"{url} SAVED TO {file}", fg=colors.GREEN)


if __name__ == "__main__":
    secho("RESET", fg=colors.MAGENTA)
    app()
