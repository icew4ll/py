import typer

app = typer.Typer()


@app.command()
def hello(name: str):
    # typer.echo(f"Hello {name}")
    typer.secho(f"hi {name}", fg="red")


@app.command()
def goodbye(name: str, formal: bool = False):
    if formal:
        typer.echo(f"Goodbye Ms. {name}. Have a good day.")
    else:
        typer.echo(f"Bye {name}!")


if __name__ == "__main__":
    # print(hello(1))
    app()
