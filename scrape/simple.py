import requests
from bs4 import BeautifulSoup


def get_links(URL):
    res = requests.get(URL)

    print(res.text)
    print(res.status_code)

    soup = BeautifulSoup(res.content, "html.parser")

    for a_href in soup.find_all("a", href=True):
        print(a_href["href"])


if __name__ == "__main__":
    URL = 'https://gitlab.com/librewolf-community/browser/appimage/-/releases'
    get_links(URL)
