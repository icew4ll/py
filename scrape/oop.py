from utils import Repo, Scraper, get_repos
from termcolor import colored


def v2():
    repos = get_repos("db.yml")
    name = "Mobian"
    # name = "Arch"
    # name = "Arch"
    select = next(iter((i for i in repos if name in i.name)))
    d1 = Repo(url=select.url, tag=select.tag, name=select.name, pre=select.pre)
    return d1


def v3(d1):
    """select tag from repos"""
    Scraper(d1).extract()


def v4(d1):
    last = None
    for last in Scraper(d1).get_links():
        pass
    print(last)
    # [print(colored(i, "yellow")) for i in Scraper(d1).get_links()]
    # https://github.com/dreemurrs-embedded/Pine64-Arch/releases/download/20210713/archlinux-pinephone-phosh-20210713.img.xz
    # https://images.mobian-project.org/pinephone/nightly/mobian-pinephone-phosh-20210101.img.gz


if __name__ == "__main__":
    print(colored('Start: ', 'yellow'))
    v4(v2())
