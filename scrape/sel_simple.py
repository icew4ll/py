from selenium import webdriver
from bs4 import BeautifulSoup
import requests
# import os
# import inspect


def get_links(URL):
    # get js links via selenium
    driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    driver.get(URL)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    tag = "pine64-pinephone.img.xz"

    def images():
        for a_href in soup.find_all("a", href=True):
            if tag in a_href["href"]:
                yield a_href["href"]

    # get first item
    url = f'https://github.com{next(iter(images()))}'
    print(url)

    # download file
    r = requests.get(url)
    file = url.split('/')[-1]

    with open(file, 'wb') as output_file:
        output_file.write(r.content)


if __name__ == "__main__":
    URL = 'https://github.com/dreemurrs-embedded/Jumpdrive/releases'
    get_links(URL)
