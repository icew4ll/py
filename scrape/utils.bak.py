from selenium import webdriver
from bs4 import BeautifulSoup
import requests
from termcolor import colored
import lzma
import os
import yaml
# from dataclasses import dataclass
from pydantic import BaseModel
from typing import List


class RepoParseError(Exception):
    """docstring for RepoParseError."""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class Repo(BaseModel):
    url: str
    tag: str


def read_yaml(file):
    file = f"{os.getcwd()}/{file}"
    with open(file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        repos: List[Repo] = [Repo(**item) for item in data]
    return repos


class Scraper(BaseModel):
    """Github_Release_Scraper"""
    repo: Repo

    def get_links(self, url: str):
        print(colored(f'Scraping: {url}', 'yellow'))
#         driver = webdriver.Firefox()
#         driver.implicitly_wait(30)
#         driver.get(rel.url)
#         soup = BeautifulSoup(driver.page_source, 'html.parser')
# 
#         for a_href in soup.find_all("a", href=True):
#             if rel.tag in a_href["href"]:
#                 yield a_href["href"]

    def get_first(self, item: Repo) -> str:
        i = item.repo
        if "github" in i.url:
            print(colored(f'Downloading: {i.url}', 'yellow'))
            url = f'https://github.com{next(iter(self.get_links(i.url)))}'
            return url
            # return url
        else:
            raise RepoParseError(value=i, message="github url support only")
        # url = f'{rel.pre}{next(iter(rel.get_links()))}'
        # print(colored(f'Downloading: {url}', 'yellow'))
        # return url

    def download(self):
        url = self.get_first()
        r = requests.get(url)
        file = url.split('/')[-1]
        with open(file, 'wb') as output_file:
            output_file.write(r.content)
        print(colored('Complete', 'green'))

    def extract(self):
        url = self.get_first()
        r = requests.get(url)
        file = url.split('/')[-1]
        img = lzma.decompress(r.content)
        with open(file[:-3], 'wb') as output_file:
            output_file.write(img)
        print(colored('Complete', 'green'))
