from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import os
import inspect


def get_links(URL):
    """install LibreWolf"""
    home = os.environ["HOME"]

    # get js links via selenium
    driver = webdriver.Firefox()
    driver.implicitly_wait(30)
    driver.get(URL)
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    def images():
        for a_href in soup.find_all("a", href=True):
            if "x86_64.AppImage" in a_href["href"]:
                yield a_href["href"]

    # get first item
    url = next(iter(images()))
    r = requests.get(url)
    file = url.split('/')[-1]
    name = file.split("-")[0]
    dest = f'{home}/bin/{name}'
    icon = "/usr/share/icons/breeze-dark/apps/64/diaspora.svg"
    print(file, dest)

    # download file
    with open(file, 'wb') as output_file:
        output_file.write(r.content)

    # make exec
    os.chmod(file, 0o775)
    os.rename(file, dest)

    # create desktop entry
    icon = "/usr/share/icons/breeze-dark/apps/48/calibre-viewer.svg"
    content = inspect.cleandoc(f'''
    [Desktop Entry]
    Encoding=UTF-8
    Version=1.0
    Type=Application
    Terminal=false
    Exec={dest}
    Name={name}
    Icon={icon}
    ''')
    entry = f'{home}/.local/share/applications/{name}.desktop'
    with open(entry, "w") as myFile:
        myFile.write(content)
    print(content)


if __name__ == "__main__":
    URL = 'https://gitlab.com/librewolf-community/browser/appimage/-/releases'
    get_links(URL)
