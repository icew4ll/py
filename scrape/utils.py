import gzip
import lzma
import os
import re
from typing import Iterator, List

import yaml
from bs4 import BeautifulSoup
from pydantic import BaseModel
from selenium import webdriver
from termcolor import colored

import requests

# from dataclasses import dataclass


class RepoParseError(Exception):
    """docstring for RepoParseError."""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class Repo(BaseModel):
    url: str
    tag: str
    name: str
    pre: str


def read_yaml(file: str):
    file = f"{os.getcwd()}/{file}"
    with open(file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def get_repos(file: str):
    data = read_yaml(file)
    repos: List[Repo] = [Repo(**item) for item in data["repos"]]
    return repos


def gunzip(source_filepath, dest_filepath, block_size=65536):
    """open two files, read from gzipped file
       write to another file in blocks
       to avoid large memory usage"""
    with gzip.open(source_filepath, "rb") as s_file, open(
        dest_filepath, "wb"
    ) as d_file:
        while True:
            block = s_file.read(block_size)
            if not block:
                break
            else:
                d_file.write(block)


class Scraper:
    """docstring for Scraper."""

    def __init__(self, repo: Repo):
        self.url = repo.url
        self.tag = repo.tag
        self.name = repo.name
        self.pre = repo.pre
        self.dest = "/tmp/build"

    def get_links(self) -> Iterator[str]:
        print(colored(f"Scraping: {self.url}", "yellow"))
        driver = webdriver.Firefox()
        driver.implicitly_wait(30)
        driver.get(self.url)
        soup = BeautifulSoup(driver.page_source, "html.parser")

        pattern = re.compile(rf"{self.tag}")
        print(colored(f"Pattern: {self.tag}", "yellow"))
        for a_href in soup.find_all("a", href=True):
            link = a_href["href"]
            if pattern.match(link):
                # print(colored(f"Match Found: {link}", "yellow"))
                yield link

    def get_first(self) -> str:
        if "github" in self.url:
            url = f"{self.pre}{next(iter(self.get_links()))}"
            return url
        else:
            raise RepoParseError(value=self, message="github url support only")

    def download(self) -> None:
        url = self.get_first()
        file = f'{self.dest}/{url.split("/")[-1]}'
        print(colored(f"Downloading: {url} \nTo: {file}", "yellow"))

        # download file
        r = requests.get(url)
        if os.path.isdir(self.dest):
            pass
        else:
            os.makedirs(self.dest)
        with open(file, "wb") as output_file:
            output_file.write(r.content)
        print(colored("Complete", "green"))

    def extract(self) -> None:
        url = self.get_first()
        file = f'{self.dest}/{url.split("/")[-1]}'
        base = os.path.splitext(file)[-2]
        ext = os.path.splitext(file)[-1]
        print(colored(f"Downloading: {url}", "yellow"))

        # create destination folder
        if os.path.isdir(self.dest):
            pass
        else:
            os.makedirs(self.dest)

        # download file
        r = requests.get(url).content

        # handle extensions
        ex1 = ".xz"
        ex2 = ".gz"

        if ext == ex1:
            print(colored(f"Extracting: {ext}", "yellow"))
            img = lzma.decompress(r)
            print(colored(f"Saving: {base}", "yellow"))
            with open(base, "wb") as output_file:
                output_file.write(img)
        if ext == ex2:
            gunzip(r, base)
        else:
            print("extension not supported")
        print(colored("Complete", "green"))
