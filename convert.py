from datetime import datetime, timedelta
from pytz import timezone
import pytz

class Date:
    def __init__(self, year, month, day, hour, minute):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute


print(Date(2020, 9, 30, 5, 28))
fmt = "%Y-%m-%d %H:%M:%S %Z%z"
# Convert local time --> UTC
# local = pytz.timezone("America/Los_Angeles")
tz = "Australia/Brisbane"
time = "2020-9-30 5:28:00"
local = pytz.timezone(tz)
# parse string into naive datetime object
naive = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
# find local timezone
local_dt = local.localize(naive, is_dst=None)
# convert to UTC
utc_dt = local_dt.astimezone(pytz.utc)
print(f"{tz} {time} --> UTC {utc_dt}")

# list all timezones
# pytz.all_timezones
# print(pytz.all_timezones)

# Convert UTC --> US/Eastern
# UTC: Monday, September 28, 2020 at 18:51:00
# EDT: 2020-09-28 14:51:00 EDT-0400
# utc dt year, month, day
tz = "Australia/Brisbane"
time = "2020-9-28 18:50:00"
local = timezone(tz)
utc_dt = datetime(2020, 9, 29, 19, 28, 0, tzinfo=pytz.utc)
# localize utc datetime
loc_dt = utc_dt.astimezone(local)
# format for output
out = loc_dt.strftime(fmt)
print(f"UTC -> {tz} {out}")
