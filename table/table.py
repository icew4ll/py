from tabulate import tabulate

l1 = "            "
column = [i for i in l1]
table = [column for i in range(3)]
print(tabulate(table, tablefmt="grid"))

# column = [i for i in range(12)]
# table = [column for i in range(3)]
# print(tabulate(table, tablefmt="grid"))
