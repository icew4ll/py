import json
import os

import yaml


class Data:
    def __init__(self):
        file = f"{os.getcwd()}/data.yml"
        with open(file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        self.data = data

    def dump_yaml(self):
        """dumps yaml"""
        print("all", json.dumps(self.data, indent=1))

    def iter(self):
        """iter yaml"""
        print("all", json.dumps(self.data[all], indent=1))
