import json
import os

import yaml

import paramiko
from paramiko_expect import SSHClientInteraction


class Data:
    def __init__(self):
        data = f"{os.getcwd()}/data.yml"
        secret = f"{os.environ['HOME']}/Documents/psp/data.yml"
        with open(data, 'r') as stream:
            try:
                data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        with open(secret, 'r') as stream:
            try:
                secret = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        self.data = data
        self.secret = secret

    def dump_yaml(self):
        """dumps yaml"""
        print("all", json.dumps(self.data, indent=1))

    def itery(self):
        """iter yaml"""
        for i in self.data:
            print(i)
    # def miko():
        # HOSTNAME = self.data[name]["ip"]
        # USERNAME = self.data[name]["user"]
        # PASSWORD = self.data[name]["pass"]
        # pass


if __name__ == "__main__":
    # Data().dump_yaml()
    Data().itery()
