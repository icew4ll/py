import os
from subprocess import run
import shutil
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

import fire
from bs4 import BeautifulSoup
from requests import get


class Builder:
    test = 10

    def htmler(self, url):
        print(f"Getting HTML from: \n{url}")
        res = get(url)
        return res.text

    def linker(self, html):
        print("Getting links...")
        soup = BeautifulSoup(html, "html.parser")
        href = soup.find_all("a", href=True)
        return (a.get("href") for a in href)

    def get_links(self):
        url = "https://dwm.suckless.org/"
        links = self.linker(self.htmler(url))
        for i in links:
            print(i)

    def cmder(self, cmds):
        """iterate list of commands"""

        def suber(cmd):
            run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)

        if isinstance(cmds, str):
            print("Running: ", cmds)
            suber(cmds)

        elif isinstance(cmds, tuple):
            for cmd in cmds:
                print("Running: ", cmd)
                suber(cmd)
        else:
            print("unknown type")

    def dir_reset(self, dir):
        """temp directory reset"""
        # remove temp dir if exists
        if os.path.isdir(dir):
            print("Removing ", dir)
            shutil.rmtree(dir)

    def dwm(self):
        url = "git://git.suckless.org/dwm"
        name = os.path.basename(url)
        dir = f"/tmp/{name}"
        file = f"{dir}/config.def.h"
        self.dir_reset(dir)
        cmds = (
            f"git clone {url} {dir}",
            f"sed -ie '/MODKEY Mod1Mask/s/Mod1Mask/Mod4Mask/' {file}",
            f'sed -ie \'/"st"/s/"st"/"rofi", "-show", "drun", "-show-icons"/\' {file}',
            f"sed -ie '/monospace:size=10/s/monospace:size=10/JetBrainsMono Nerd Font Mono:size=14/' {file}",
            f"sudo make -C {dir} clean install",
            # f"(cd {dir} && sudo make clean install)",
        )
        self.cmder(cmds)


if __name__ == "__main__":
    print("=" * 40)
    s1 = set(dir(Builder()))
    s2 = {
        "dir_reset",
        "cmder",
    }
    cmds = [i for i in s1 - s2 if "__" not in i]
    cmd_completer = WordCompleter(cmds)
    while True:
        text = prompt(
            "Input commands: ",
            completer=cmd_completer,
            complete_while_typing=False,
        )
        print(f"You said: {text}")
        # instantiate class and run method
        # b = build()
        res = getattr(Builder(), text)()
