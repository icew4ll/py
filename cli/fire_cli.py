# note: sudo does not work in watchexec
import os
import shutil
from subprocess import run

import fire
from bs4 import BeautifulSoup
from requests import get


class Builder:
    home = os.environ["HOME"]
    bin_dir = f"{home}/bin"
    config = f"{home}/.config"
    dot_url = "https://gitlab.com/users/icew4ll/public"
    dot_dir = f"{home}/m/public"

    def htmler(self, url):
        print(f"Getting HTML from: \n{url}")
        res = get(url)
        return res.text

    def linker(self, html):
        print("Getting links...")
        soup = BeautifulSoup(html, "html.parser")
        href = soup.find_all("a", href=True)
        return (a.get("href") for a in href)

    def get_links(self, url):
        links = self.linker(self.htmler(url))
        for i in links:
            print(i)

    def soft_linker(self, src, dest):
        """soft links src to dest"""
        print(f"Linking {src} to {dest}")
        if os.path.isfile(dest):
            os.unlink(dest)
        os.symlink(src, dest)

    def suber(self, cmd):
        run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)

    def cmder(self, cmds):
        """iterate list of commands"""
        for cmd in cmds:
            print("Running: ", cmd)
            self.suber(cmd)

    def dir_reset(self, dir):
        """temp directory reset"""
        # remove temp dir if exists
        if os.path.isdir(dir):
            print("Removing ", dir)
            shutil.rmtree(dir)

    def dwm(self):
        url = "git://git.suckless.org/dwm"
        name = os.path.basename(url)
        dir = f"/tmp/{name}"
        file = f"{dir}/config.def.h"
        self.dir_reset(dir)
        cmds = (
            f"git clone {url} {dir}",
            f"sed -ie '/MODKEY Mod1Mask/s/Mod1Mask/Mod4Mask/' {file}",
            f'sed -ie \'/"st"/s/"st"/"rofi", "-show", "drun", "-show-icons"/\' {file}',
            f"sed -ie '/monospace:size=10/s/monospace:size=10/JetBrainsMono Nerd Font Mono:size=14/' {file}",
            f"sudo make -C {dir} clean install",
        )
        self.cmder(cmds)

    def rust(self):
        if shutil.which("rustc"):
            print("rust installed")
            cmd = [
                "cargo install cargo-update cargo-edit cargo-outdated skim git-delta bat lsd zoxide fd-find ripgrep watchexec bb bottom shotgun",
                "cargo install-update -a"
            ]
            self.cmder(cmd)
        else:
            print("rust not installed")
            self.suber("curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh")

    def rofi(self):
        """rofi install"""
        url = "https://github.com/DaveDavenport/rofi"
        name = os.path.basename(url)
        dir = f"{self.bin_dir}/build/{name}"
        self.dir_reset(dir)
        # git clone
        cmd = f"git clone --recursive {url} {dir}"
        self.suber(cmd)
        os.chdir(dir)
        cmds = [
            "git pull && git submodule update --init && autoreconf -i",
            "mkdir build",
            "./configure",
            "make",
            "sudo make install",
        ]
        self.cmder(cmds)


if __name__ == "__main__":
    print("=" * 40)
    fire.Fire(Builder)
