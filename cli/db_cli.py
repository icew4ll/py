import ipaddress
import os

from sqlalchemy import Column, Integer, MetaData, String, Table, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(f'sqlite:///{os.environ["db"]}')
Base = declarative_base()


# table metadata
class Server(Base):
    __tablename__ = "Servers"
    id = Column(Integer, primary_key=True)
    rack_id = Column(Integer)
    name = Column(String)
    ip = Column(Integer)
    eth = Column(String)

    def __repr__(self):
        return f"<Server(name='{self.name}', rack_id='{self.rack_id}', ip='{self.ip}, eth='{self.eth}')>"


# Sessionn = ORM's handle to the database
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

# querying
# q1 = session.query(Server).filter_by(rack_id='22')
# print([i for i in q1])

# id, name = session.query(Server.id, Server.name).filter_by(rack_id='22').first()
# print(id, name)

# query filter
# for id, rack_id, name, ip, eth in session.query(
#     Server.id, Server.rack_id, Server.name, Server.ip, Server.eth
# ).filter_by(rack_id="22"):
#     print(id, rack_id, name, eth, str(ipaddress.ip_address(int(ip))))

# query names
# for i in session.query(Server).order_by(Server.name):
#     print(i.name)

# for i in session.query(Server):
#     print(i.name)

# for i in session.query(Server.name):
#     print(i)

# for i in session.query(Server.name).filter_by(name="webltw02"):
#     print(i)

# for i in session.query(Server).filter(Server.name=="webltw02"):
#     print(i.name)

# for i in session.query(Server).filter(Server.name.like('%web%')):
#     print(i.name)

for i in session.query(Server):
    print(f"{i.rack_id},{i.name},{str(ipaddress.ip_address(int(i.ip)))},{i.eth}")
