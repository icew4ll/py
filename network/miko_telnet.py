import ipaddress
import json
import os
import re
import sys
import traceback

import paramiko
import yaml
from paramiko_expect import SSHClientInteraction


def read_yml(yml):
    with open(yml, 'r') as stream:
        data = yaml.safe_load(stream)
    return data
    # print(data['config']['user'])


def telnet():
    data = read_yml(os.environ["data"])
    print(data)
    results = []
    HOST = data["config"]["ab10"]["ip"]
    USER = data["config"]["ab10"]["user"]
    PASS = data["config"]["ab10"]["pass"]
    print(USER)

    def sender(cmd, exp):
        """docstring for sender"""
        interact.send(cmd)
        interact.expect(exp)
        results.append(repr(interact.current_output_clean))

    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=HOST, username=USER, password=PASS)

        with SSHClientInteraction(client, timeout=7, display=True) as interact:
            # setup
            PROMPT = r"pspinc@ambk10:~\$\s+"
            # ROOT_PROMPT = r"root@serveradmin:~\#\s+"
            # PASS_PROMPT = r"\[sudo\] password for pspinc:\s+"
            interact.expect(PROMPT)

            # login as root
            sender("ls", PROMPT)
            # sender("sudo su -", PASS_PROMPT)
            # sender(PASSWORD, ROOT_PROMPT)
            # sender(QUERY, ROOT_PROMPT)
            interact.send("exit")
            interact.expect()

    except Exception:
        traceback.print_exc()
    finally:
        try:
            client.close()
        except Exception:
            pass

    # Process Results:
    res = results[-1].split("\\n")
    print(res)


if __name__ == "__main__":
    print("=" * 79)
    telnet()
