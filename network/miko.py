import ipaddress
import json
import os
import re
import sys
import traceback

import paramiko
import yaml
from paramiko_expect import SSHClientInteraction


class Servers:
    def __init__(self):
        with open(os.environ["data"]) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        self.data = data["servers"]

    def miko(self, name):
        HOSTNAME = self.data[name]["ip"]
        USERNAME = self.data[name]["user"]
        PASSWORD = self.data[name]["pass"]

        results = []

        def sender(cmd, exp):
            """docstring for sender"""
            interact.send(cmd)
            interact.expect(exp)
            results.append(repr(interact.current_output_clean))

        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=HOSTNAME, username=USERNAME, password=PASSWORD)

            with SSHClientInteraction(client, timeout=7, display=True) as interact:
                # setup
                PROMPT = r"pspinc@serveradmin:~\$\s+"
                ROOT_PROMPT = r"root@serveradmin:~\#\s+"
                PASS_PROMPT = r"\[sudo\] password for pspinc:\s+"
                DB = "racktables_db"
                OBJ = "Object"
                IPA = "IPv4Allocation"
                QUERY = f'mysql -uroot -p{PASSWORD} -e "use {DB};select o.id, o.name, i.ip from {IPA} i inner join {OBJ} o on o.id = i.object_id;"'
                interact.expect(PROMPT)

                # login as root
                sender("ls", PROMPT)
                sender("sudo su -", PASS_PROMPT)
                sender(PASSWORD, ROOT_PROMPT)
                sender(QUERY, ROOT_PROMPT)
                interact.send("exit")
                interact.expect()

        except Exception:
            traceback.print_exc()
        finally:
            try:
                client.close()
            except Exception:
                pass

        # Process Results:
        res = results[-1].split("\\n")
        # id, name, ip
        reg = re.compile(r"\s+(\d+)\s+\|\s+([a-zA-Z0-9-]+)\s+\|\s+(\d+)\s\|")

        results = {}
        for i in res:
            m = reg.search(i)
            if m:
                ip = str(ipaddress.ip_address(int(m.group(3))))
                name = m.group(2)
                id = m.group(1)
                results[name] = [id, ip]

        print(json.dumps(results, indent=1))

        # dump results to yml
        file = os.environ["write"]
        with open(file, "w") as f:
            yaml.dump(results, f)


if __name__ == "__main__":
    print("=" * 79)
    Servers().miko("racktables")
