import os
import sys

from pexpect import pxssh


class Server:
    # init is called when new object of class is instantiated
    # aka "constructors" in OOP
    def __init__(self, ip, user, pwd):
        self.ip = ip
        self.user = user
        self.pwd = pwd

    def ssh(self):
        """sshing"""

        def sender(cmd):
            """send ssh commands"""
            s.sendline(cmd)  # run a command
            s.prompt()  # match the prompt
            print(s.before)  # print everything before the prompt.

        try:
            s = pxssh.pxssh(
                options={
                    "StrictHostKeyChecking": "no",
                    "UserKnownHostsFile": "/dev/null",
                },
                encoding="utf-8",
            )
            s.login(self.ip, self.user, self.pwd)
            sender("uptime")
            sender("whoami")
            s.logout()
        except pxssh.ExceptionPxssh as e:
            print("pxssh failed on login.")
            print(e)


s1 = Server("216.230.243.243", os.environ["UI"], pwd=os.environ["PG"])
s1.ssh()


# pop commands
# LIST
# RETR X
# DELE X
# QUIT
