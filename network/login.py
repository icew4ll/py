# importing the module
import json
import os

import yaml
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

file = os.environ["write"]
with open(file, "r") as stream:
    try:
        data = yaml.safe_load(stream)
        # sns = SimpleNamespace(**data)
    except yaml.YAMLError as exc:
        print(exc)

print(data["webltwTEST-V4-FAILOVER-TEST | iscsi0"])

completer = WordCompleter(data)
while True:
    text = prompt(
        "Servers:",
        completer=completer,
        complete_while_typing=True,
    )
    print(data[text])
