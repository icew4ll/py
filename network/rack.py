import json
import os
import sys
# import redexpect
import traceback

import paramiko
import yaml
from paramiko_expect import SSHClientInteraction

# import pexpect
# from pexpect import pxssh

# import pexpect


class Data:
    def __init__(self):
        with open(os.environ["data"]) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        self.data = data

    def read_yaml(self):
        """open yaml"""
        print("all", json.dumps(self.data, indent=1))
        print("servers", json.dumps(self.data["servers"], indent=1))
        print("jb05", json.dumps(self.data["servers"]["jb05"], indent=1))

    def miko(self, name):
        ip = self.data["servers"][name]["ip"]
        user = self.data["servers"][name]["user"]
        pw = self.data["servers"][name]["pass"]

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(
            hostname=ip,
            username=user,
            password=pw,
        )
        stdin, stdout, stderr = ssh_client.exec_command("ls")

        out = stdout.read().decode().strip()
        error = stderr.read().decode().strip()
        # if self.log_level:
        #     logger.info(out)
        if error:
            raise Exception("There was an error pulling the runtime: {}".format(error))
        ssh_client.close()

        return out

    def red(self, name):
        hostname = self.data["servers"][name]["ip"]
        username = self.data["servers"][name]["user"]
        passwd = self.data["servers"][name]["pass"]
        expect = redexpect.RedExpect()

        print(hostname)
        expect.login(
            ip=hostname,
            username=username,
            password=passwd,
            allow_agent=True,
            timeout=1.5,
        )
        print(expect.command("whoami", remove_newline=True))
        expect.exit()

    def miko_ex(self, name):
        HOSTNAME = self.data["servers"][name]["ip"]
        USERNAME = self.data["servers"][name]["user"]
        PASSWORD = self.data["servers"][name]["pass"]
        PROMPT = "pspinc@serveradmin:~\$\s+"

        # Use SSH client to login
        try:
            # Create a new SSH client object
            client = paramiko.SSHClient()

            # Set SSH key parameters to auto accept unknown hosts
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            # Connect to the host
            client.connect(hostname=HOSTNAME, username=USERNAME, password=PASSWORD)

            # Create a client interaction class which will interact with the host
            with SSHClientInteraction(client, timeout=10, display=True) as interact:
                interact.expect(PROMPT)

                # Run the first command and capture the cleaned output, if you want
                # the output without cleaning, simply grab current_output instead.
                interact.send("uname -a")
                interact.expect(PROMPT)
                cmd_output_uname = interact.current_output_clean

                # Now let's do the same for the ls command but also set a timeout for
                # this specific expect (overriding the default timeout)
                interact.send("ls -l /")
                interact.expect(PROMPT, timeout=5)
                cmd_output_ls = interact.current_output_clean

                # To expect multiple expressions, just use a list.  You can also
                # selectively take action based on what was matched.

                # Method 1: You may use the last_match property to find out what was
                # matched
                interact.send("/vagrant/examples/paramiko_expect-demo-helper.py")
                interact.expect([PROMPT, "Please enter your name: "])
                if interact.last_match == "Please enter your name: ":
                    interact.send("Fotis Gimian")
                    interact.expect(PROMPT)

                # Method 2: You may use the matched index to determine the last match
                # (like pexpect)
                interact.send("/vagrant/examples/paramiko_expect-demo-helper.py")
                found_index = interact.expect([PROMPT, "Please enter your name: "])
                if found_index == 1:
                    interact.send("Fotis Gimian")
                    interact.expect(PROMPT)

                # Send the exit command and expect EOF (a closed session)
                interact.send("exit")
                interact.expect()

            # Print the output of each command
            print("-" * 79)
            print("Cleaned Command Output")
            print("-" * 79)
            print("uname -a output:")
            print(cmd_output_uname)
            print("ls -l / output:")
            print(cmd_output_ls)

        except Exception:
            traceback.print_exc()
        finally:
            try:
                client.close()
            except Exception:
                pass

    def miko_ex2(self, name):
        HOSTNAME = self.data["servers"][name]["ip"]
        USERNAME = self.data["servers"][name]["user"]
        PASSWORD = self.data["servers"][name]["pass"]
        PROMPT = "pspinc@serveradmin:~\$\s+"
        PW_PROMPT = ".*password.*"
        ROOT_PROMPT = "root@serveradmin:~\$\s+"
        ANY = ".*"

        try:
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=HOSTNAME, username=USERNAME, password=PASSWORD)

            with SSHClientInteraction(client, timeout=10, display=True) as interact:
                interact.expect(PROMPT)
                # login as root
                interact.send("sudo su -")
                interact.expect(PW_PROMPT)
                print(interact.current_output_clean)

                interact.send(PASSWORD)
                interact.expect(ANY)
                print(interact.current_output_clean)

                interact.send("ls")
                interact.expect(ANY)
                print(interact.current_output_clean)

                interact.send("exit")
                interact.expect(ANY)
                print(interact.current_output_clean)

                interact.send("exit")
                interact.expect()

        except Exception:
            traceback.print_exc()
        finally:
            try:
                client.close()
            except Exception:
                pass


if __name__ == "__main__":
    # Data("data").read_yaml()
    # Data().read_yaml()
    # print(Data().miko("racktables"))
    print("=" * 79)
    Data().miko_ex2("racktables")
