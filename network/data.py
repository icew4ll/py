import json
import os
import sys
import pexpect
import traceback

import paramiko
import yaml
from paramiko_expect import SSHClientInteraction
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

# import pexpect
# from pexpect import pxssh

# import pexpect


class Data:
    def __init__(self, file):
        file = os.environ["data"]
        with open(file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        self.data = data

    def servers(self):
        """docstring for items"""
        return self.data["servers"]

    def interact(self, name):
        d = self.servers()
        ip = d[name]["ip"]
        user = d[name]["user"]
        pw = d[name]["pass"]
        try:
            ssh_newkey = "Are you sure you want to continue connecting"
            # my ssh command line
            p = pexpect.spawn(f"ssh {user}@{ip}", encoding="utf-8")
            i = p.expect([ssh_newkey, "password:", pexpect.EOF])
            if i == 0:
                print("I say yes")
                p.sendline("yes")
                i = p.expect([ssh_newkey, "password:", pexpect.EOF])
            if i == 1:
                print("I give password")
                p.sendline(pw)
                # p.expect("root")
                # p.expect(pexpect.EOF)
            elif i == 2:
                print("I either got key or connection timeout")
                pass
            print(p.before)
            p.interact()
        except Exception as e:
            print("pxssh failed on login.")
            print(e)


if __name__ == "__main__":
    d = Data("data")
    cmd_completer = WordCompleter(d.servers())
    while True:
        text = prompt(
            "Input commands: ",
            completer=cmd_completer,
            complete_while_typing=False,
        )
        print(f"You said: {text}")
        d.interact(text)
