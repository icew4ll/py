import os
import sys

# from pexpect import pxssh
import pexpect


class Server:
    # init is called when new object of class is instantiated
    # aka "constructors" in OOP
    def __init__(self, ip, email, pwd):
        self.ip = ip
        self.email = email
        self.pwd = pwd
        self.ab_ip = "216.230.243.243"
        self.ab_user = os.environ["UI"]
        self.ab_pwd = os.environ["PG"]

    def ssh(self):
        """sshing"""

        def sender(cmd):
            """send ssh commands"""

        try:
            s = pexpect.spawn(f"ssh {self.ab_user}@{self.ab_ip}", encoding="utf-8",)
            s.logfile = sys.stdout
            s.sendline("whoami")
            s.expect("$")
            print(s.before)  # print everything before the prompt.

            # s.login(self.ab_ip, self.ab_user, self.ab_pwd)
            # s.sendline(f"telnet {self.ip} 110")
            # s.expect('+OK POP3 server ready')
            # print(s.before)
            # s.sendline("QUIT")
            # s.expect("+O.*")
            # print(s.before)
            s.close()
        except Exception as e:
            print(str(e))


s1 = Server(os.environ["mti"], os.environ["mtm"], os.environ["mtp"])
s1.ssh()


# pop commands
# LIST
# RETR X
# DELE X
# QUIT

# s.sendline(cmd)  # run a command
# s.prompt()  # match the prompt
# print(s.before)  # print everything before the prompt.

# child = pexpect.spawn('ftp ftp.openbsd.org')
# child.expect('Name .*: ')
# child.sendline('anonymous')
# child.expect('Password:')
# child.sendline('noah@example.com')
# child.expect('ftp> ')
# child.sendline('lcd /tmp')
# child.expect('ftp> ')
# child.sendline('cd pub/OpenBSD')
# child.expect('ftp> ')
# child.sendline('get README')
# child.expect('ftp> ')
# child.sendline('bye')

# try:
#     s = pxssh.pxssh(
#         options={
#             "StrictHostKeyChecking": "no",
#             "UserKnownHostsFile": "/dev/null",
#         },
#         encoding="utf-8",
#     )
#     s.logfile = sys.stdout
#     s.login(self.ab_ip, self.ab_user, self.ab_pwd)
#     s.sendline(f"telnet {self.ip} 110")
#     s.expect('+OK POP3 server ready')
#     print(s.before)
#     s.sendline("QUIT")
#     s.expect("+O.*")
#     print(s.before)
#     # sender(f"telnet {self.ip} 110")
#     # sender("QUIT\n")
#     s.logout()
# except pxssh.ExceptionPxssh as e:
#     print("pxssh failed on login.")
#     print(e)
