class Decorator:
    def __init__(self, function):
        self.function = function

    def __call__(self):
        # add code before function call
        print("before")
        self.function()
        # add code after function call
        print("after")


class DecoratorArgs:
    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        # add code before function call
        print("before")
        self.function(*args, **kwargs)
        # add code after function call
        print("after")


class DecoratorReturn:
    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        # add code before function call
        print("before")
        result = self.function(*args, **kwargs)
        # add code after function call
        return result


@Decorator
def fun():
    print("Test")


@DecoratorArgs
def fun_args(name, message="Hello"):
    print(f"{message}, {name}")


@DecoratorReturn
def get_square(n):
    print("given number is ", n)
    return n * n


if __name__ == "__main__":
    # fun()
    # Test
    # before
    # Test

    # fun_args("test", "hello")
    # before
    # hello, test
    # after

    print("Square of number is:", get_square(195))
    # before
    # given number is  195
    # Square of number is: 38025
