realm_list = [
    "Globally  ",
    "50 580 laboratory-confirmed ",
    "(1527 new) ",
    "  ",
    "China ",
    "50 054 laboratory-confirmed ",
    "(1506 new) ",
    "  1524 deaths (121 new) † ",
    " ",
    " Outside of China ",
    "      526 laboratory-confirmed ",
    "      (21 new) ",
    "        25 countries (1 new) ",
    "           2 deaths ",
    " ",
]

# function indexer
delim = "  "
delim2 = " "


def find(lst, y, z):
    result = []
    for i, x in enumerate(lst):
        if x == y or x == z:
            result.append(i)
    return result


matches2 = find(realm_list, delim, delim2)

# list comprehension indexer
matches = [i for i, x in enumerate(realm_list) if x == "  " or x == " "]

print(matches)
print(matches2)

realm_total = "".join(
    [i for i in realm_list[: matches[0]][1].split(" ") if "confirmed" not in i if i]
)
realm_d = {"realm_total": realm_total}
print(realm_d)

# dictionary comprehension:
sections = {
    "realm": ["Globally  ", "50 580 laboratory-confirmed ", "(1527 new) "],
    "china": [
        "China ",
        "50 054 laboratory-confirmed ",
        "(1506 new) ",
        "  1524 deaths (121 new) † ",
    ],
    "outside": [
        " Outside of China ",
        "      526 laboratory-confirmed ",
        "      (21 new) ",
        "        25 countries (1 new) ",
        "           2 deaths ",
    ],
}
{print(k, v) for k, v in sections.items()}
