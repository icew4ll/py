"""
# Install librewolf appimage
https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/94.0.2-2/LibreWolf.x86_64.AppImage
"""
# from typing import Tuple
from utils import green
from utils import Scrape
from utils import yellow


def main():
    """main"""
    yellow("start")
    url = "https://gitlab.com/librewolf-community/browser/appimage/-/releases"
    tag = ".*LibreWolf.x86_64.AppImage$"
    icon = "/usr/share/icons/breeze-dark/apps/48/calibre-viewer.svg"
    Scrape(url, tag, icon).appimage()
    green("complete")


if __name__ == "__main__":
    main()
