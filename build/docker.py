"""
simple install example
"""

from dataclasses import dataclass
from typing import Tuple
from utils import green, cmd_runner


@dataclass(frozen=True)
class Gpg:
    """Gpg"""

    url: str
    key: str
    src: str
    repo: str
    rel: str


@dataclass()
class Docker:
    """Build Docker"""

    pre: Tuple[str, ...] = (
        "docker",
        "docker-engine",
        "docker.io",
        "containerd",
        "runc",
    )

    preqs: Tuple[str, ...] = (
        "apt-transport-https",
        "ca-certificates",
        "curl",
        "gnupg",
        "lsb-release",
    )

    gpg: Gpg = Gpg(
        url="https://download.docker.com/linux/ubuntu/gpg",
        key="/usr/share/keyrings/docker-archive-keyring.gpg",
        src="/etc/apt/sources.list.d/docker.list",
        repo="stable",
        # rel="focal",
        rel="$(lsb_release -cs)",
    )

    def install(self):
        """Install Docker"""

        return (
            f"sudo apt-get remove {' '.join(self.pre)}",
            f"sudo apt-get install {' '.join(self.preqs)}",
            f"sudo rm {self.gpg.key}",
            f"curl -fsSL {self.gpg.url} | sudo gpg --dearmor -o {self.gpg.key}",
            (
                'echo "deb [arch=$(dpkg --print-architecture)'
                f" signed-by={self.gpg.key}]"
                f' {self.gpg.url.replace("/gpg", "")}'
                f' {self.gpg.rel} {self.gpg.repo}"'
                f" | sudo tee {self.gpg.src} > /dev/null"
            ),
            "sudo apt-get update",
            "sudo apt-get -y install docker-ce docker-ce-cli containerd.io",
            "sudo groupadd docker",
            "sudo usermod -aG docker $USER",
            "newgrp docker",
            "docker run hello-world",
        )


def main() -> None:
    """main loop"""
    print(green("\nStart"))
    dock1 = Docker()
    cmd_runner(dock1.install())
    print(green("\nCOMPLETE"))


if __name__ == "__main__":
    main()
