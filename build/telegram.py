"""
# Install telegram
https://github.com/telegramdesktop/tdesktop/releases/download/v3.2.8/tsetup.3.2.8.beta.tar.xz

# how to scrape
https://medium.com/@hoppy/how-to-test-or-scrape-javascript-rendered-websites-with-python-selenium-a-beginner-step-by-c137892216aa

# streaming download
https://365datascience.com/tutorials/python-tutorials/python-requests-package/
"""

from dataclasses import dataclass

# from typing import Tuple
from utils import green, yellow, cmd_runner, home
from time import sleep
from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import lzma
import tarfile
from contextlib import closing


@dataclass
class Scrape:
    """scrape"""

    url: str = "https://github.com/telegramdesktop/tdesktop/releases"
    tag: str = "tar.xz"
    dir: str = "/tmp"
    prefix: str = "https://github.com/"

    def get_links(self):
        """start driver"""
        yellow("Starting driver")
        with webdriver.Firefox() as driver:
            driver.get(self.url)
            driver.implicitly_wait(30)
            soup = BeautifulSoup(driver.page_source, "html.parser")

            def find_links():
                for a_href in soup.find_all("a", href=True):
                    if self.tag in a_href["href"]:
                        yield a_href["href"]

            # [print(i) for i in find_links()]
            url = f"{self.prefix}{next(iter(find_links()))}"
            response = requests.get(url)
            file = f'{self.dir}/{url.rsplit("/", maxsplit=1)[-1]}'
            print(file)

            # download file
            # 'wb': Write-only mode in binary format;
            with open(file, "wb") as output_file:
                output_file.write(response.content)

            # extract xz
            with lzma.open(file) as temp:
                # extract tar
                with tarfile.open(fileobj=temp) as tar:
                    tar.extractall(self.dir)

        green("Driver Closed")


def main():
    """main"""
    scrape1 = Scrape()
    scrape1.get_links()
    green("complete")


if __name__ == "__main__":
    main()
