"""
simple multiprocessing example
"""

from multiprocessing import Process


def main(name):
    """hello"""
    print("hello", name)


if __name__ == "__main__":
    p = Process(target=main, args=("bob",))
    p.start()
    p.join()
