"""install kitty
https://sw.kovidgoyal.net/kitty/build/
fails:
/tmp/python-build.20211019011605.3132551/Python-3.10.0/./Modules/timemodule.c:341: undefined reference to `pthread_getcpuclockid'
collect2: error: ld returned 1 exit status
"""

from dataclasses import dataclass
from typing import Tuple
from utils import green, cmd_runner, home


@dataclass()
class Kitty:
    """Build Kitty"""

    dir: str = "/tmp/kitty"
    bin: str = f"{home}/bin"
    url: str = "https://github.com/kovidgoyal/kitty"
    deps: Tuple[str, ...] = ("librsync-dev",)

    def cmds(self):
        """cmds"""
        return (
            f"sudo apt install {' '.join(self.deps)}",
            # f"exists -d {self.dir} && rm -rf {self.dir}",
            f"mkdir {self.dir}",
            f"git clone {self.url} {self.dir}",
            f"cd {self.dir} && make",
            f"mv {self.dir}/kitty/launcher/kitty {self.bin}",
        )


def main():
    """main"""
    print(green("\nStart"))
    obj = Kitty()
    cmd_runner(obj.cmds())
    print(green("\nCOMPLETE"))


if __name__ == "__main__":
    main()
