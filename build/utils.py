"""
utils
# https://www.geeksforgeeks.org/print-colors-python-terminal/
"""
import inspect
import os
import re
import subprocess
from dataclasses import dataclass
from dataclasses import field
from typing import Generator
from typing import Iterator
from typing import List

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

home = os.environ["HOME"]


def cmd_runner(cmds):
    """cmd runner"""
    for i in cmds:
        print(yellow(i))
        # subprocess.call(i, shell=True, executable=f"{home}/bin/ion")
        subprocess.call(i, shell=True, executable="/bin/bash")


def red(text: str) -> None:
    """yellow text"""
    print(f"\033[91m {text}\033[00m")


def yellow(text: str) -> None:
    """yellow text"""
    print(f"\033[93m {text}\033[00m")


def purple(text: str) -> None:
    """yellow text"""
    print(f"\033[95m {text}\033[00m")


def green(text: str) -> None:
    """green text"""
    print(f"\033[92m {text}\033[00m")


@dataclass
class Scrape:
    """scrape"""

    url: str
    tag: str
    icon: str
    dir: str = "/tmp"
    home: str = os.environ["HOME"]

    def get_links(self) -> Iterator[str]:
        """parse"""
        with webdriver.Firefox() as driver:
            driver.get(self.url)
            driver.implicitly_wait(30)
            soup = BeautifulSoup(driver.page_source, "html.parser")
        return (a_href["href"] for a_href in soup.find_all("a", href=True))

    def get_tag(self) -> str:
        """return first tag match"""
        pattern = re.compile(self.tag)
        return next(
            iter((item for item in self.get_links() if re.match(pattern, item)))
        )

    def appimage(self) -> None:
        """process"""
        url = self.get_tag()
        response = requests.get(url)
        # extract base name
        base = url.rsplit("/", maxsplit=1)[-1]
        file = f"{self.dir}/{base}"
        # extract bin name
        name = base.rsplit(".")[0]
        out = f"{self.home}/bin/{name}"

        yellow(f"Downloading...\nfrom: {url}\nto: {file}")
        # download file
        # 'wb': Write-only mode in binary format;
        with open(file, "wb") as output_file:
            output_file.write(response.content)

        # make exec
        yellow(f"Chmod 775...\n{file}")
        os.chmod(file, 0o775)
        yellow(f"Moving...\nfrom: {file}\nto: {out}")
        os.rename(file, out)

        # create desktop entry
        content = inspect.cleandoc(
            f"""
        [Desktop Entry]
        Encoding=UTF-8
        Version=1.0
        Type=Application
        Terminal=false
        Exec={out}
        Name={name}
        Icon={self.icon}
        """
        )

        # write file
        yellow("Writing desktop file...")
        file = f"{self.home}/.local/share/applications/{name}.desktop"
        with open(file, "w") as output:
            output.write(content)
