# import os
# import shutil
# import inspect
from subprocess import run
from typing import List
from dataclasses import dataclass


@dataclass(frozen=True)
class Gpg:
    url: str
    key: str

    def install(self) -> str:
        return f"curl -fsSL {self.url} | sudo gpg --dearmor -o {self.key}"


@dataclass(frozen=True)
class Preqs:
    # pkgs: list = field(default_factory=list)
    pkgs: List[str]

    def install(self) -> str:
        return f"sudo apt-get install {' '.join(self.pkgs)}"


def cmds() -> List[str]:
    pkgs = ["apt-transport-https", "ca-certificates", "curl", "gnupg", "lsb-release"]
    cmds = [
        "sudo apt-get update",
        Preqs(pkgs).install(),
        Gpg(
            "https://download.docker.com/linux/ubuntu/gpg",
            "/usr/share/keyrings/docker-archive-keyring.gpg",
        ).install(),
    ]
    return cmds


def text_yellow(s: str) -> str:
    return f"\033[93m {s}\033[00m"


def text_green(s: str) -> str:
    return f"\033[92m {s}\033[00m"


def suber(cmd: str) -> None:
    run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)


def cmder(cmds: List[str]) -> None:
    """iterate list of commands"""
    for cmd in cmds:
        print(text_yellow(f"Running: {cmd}"))
        suber(cmd)


def main() -> None:
    cmder(cmds())
    print(text_green("\nCOMPLETE"))


if __name__ == "__main__":
    main()
