import asyncio

from multiprocessing import Value, Process, set_start_method
import multiprocessing as mp
from ctypes import c_wchar_p


class Worker:
    def __init__(self):
        # self.stderr = Value(c_wchar_p, 'Hello')
        self.man = mp.Manager()
        self.dict = self.man.dict(err="Hello")
        print(f"Original: {self.dict}")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, trace):
        pass

    async def _mainloop(self):
        raise NotImplementedError()

    def start(self):
        process = Process(target=_start_process, args=(self.dict,))
        process.start()


def handle_error(d, exception: Exception):
    print(f"Before logging: {d}")
    d["err"] = d["err"] + ", World!"
    print(f"Handling error: {d}")


def _start_process(d):
    async def start_coroutine(d):
        try:
            raise RuntimeError("crud")

        except Exception as e:
            handle_error(d, e)

        finally:
            pass

    print(f"Before event loop: {d}")
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(start_coroutine(d))


if __name__ == "__main__":
    set_start_method("spawn")

    worker = Worker()
    worker.start()

    input(">")
    print(f"Outside: {worker.dict}")
