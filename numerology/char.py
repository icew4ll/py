"""
numerology calculator
"""

import sys
import tty
import termios


def get_ch():
    """get ch"""
    file = sys.stdin.fileno()
    old_settings = termios.tcgetattr(file)
    try:
        tty.setraw(sys.stdin.fileno())
        char = sys.stdin.read(1)
    finally:
        termios.tcsetattr(file, termios.TCSADRAIN, old_settings)
    return char


def getpass(prompt="Password: "):
    """pass"""
    file = sys.stdin.fileno()
    old = termios.tcgetattr(file)
    new = termios.tcgetattr(file)
    new[3] = new[3] & ~termios.ECHO  # lflags
    try:
        termios.tcsetattr(file, termios.TCSADRAIN, new)
        passwd = input(prompt)
    finally:
        termios.tcsetattr(file, termios.TCSADRAIN, old)
    return passwd


def main():
    """main"""
    result = []
    while 1:
        char = get_ch()
        if char == "q":
            break
        result.append(char)
        print("".join(result))
        # print(char,end=" ")


def green(text: str) -> str:
    """green text"""
    return f"\033[92m {text}\033[00m"


if __name__ == "__main__":
    print(green("START"))
    main()
    print(green("COMPLETE"))

    # redwing onion
    # russian red
    # shallots onion
    # salad green
