"""
numerology calculator
"""

from rich.console import Console
from rich.table import Table
import sys
import tty
import termios


def get_ch():
    """get ch"""
    file = sys.stdin.fileno()
    old_settings = termios.tcgetattr(file)
    try:
        tty.setraw(sys.stdin.fileno())
        char = sys.stdin.read(1)
    finally:
        termios.tcsetattr(file, termios.TCSADRAIN, old_settings)
    return char


def main():
    """main"""
    result = []
    while 1:
        char = get_ch()
        if char == "2":
            size = len(result)
            if size < 1:
                continue
            result.pop(size - 1)
        elif char == "1":
            break
        else:
            result.append(char)
        print(f"{''.join(result)} test")
        # print(char,end=" ")


def green(text: str) -> str:
    """green text"""
    return f"\033[92m {text}\033[00m"


if __name__ == "__main__":
    print(green("START"))
    main()
    print(green("COMPLETE"))

    # redwing onion
    # russian red
    # shallots onion
    # salad green
