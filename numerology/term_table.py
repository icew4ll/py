"""terminal_table"""

from terminal_table import Table
from ansi_colours import AnsiColours as Colour

rows = (
    (1, "Johnathon", "Pollitt", "Seres"),
    (2, "Nerita", "Beetham", "Guanshan"),
    [3, "Celia", "Cawsby", "Łaskarzew"],
    [4, "Carolin", "Muggleston", "Beishan"],
    (5, "Homere", "Caird", "Shuangzhu"),
    (6, "Conrado", "Wethey", "Yezhi"),
    (7, "Winifred", "Malloch", "Orekhovo - Borisovo Severnoye"),
    (8, "Ag", "Wardley", "Buenos Aires"),
    (9, "Shelby", "Janiak", "Skänninge"),
    (10, "Sully", "McIlmurray", "Huxiaoqiao"),
)


table = Table.create(
    rows,
    ("ID", "First Name", "Last Name", "City"),
    header_colour=Colour.cyan,
    column_colours=(Colour.green,),
)
print(table)
