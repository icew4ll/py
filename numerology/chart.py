"""chart"""
from dataclasses import dataclass
from typing import Tuple
from terminal_table import Table
from ansi_colours import AnsiColours as Colour


@dataclass(frozen=True)
class Chart:
    """table"""

    rows: Tuple[Tuple[str, ...], ...] = (
        ("a", "b", "c", "d", "e", "f", "g", "h", "i"),
        ("j", "k", "l", "m", "n", "o", "p", "q", "r"),
        ("s", "t", "u", "v", "w", "x", "y", "z", ""),
    )

    header: Tuple[int, ...] = tuple(i for i in range(1, 10))

    def render(self) -> None:
        """render"""
        return Table.create(
            self.rows,
            self.header,
            header_colour=Colour.cyan,
        )


def main():
    """main"""
    char = Chart()
    print(char.render())


if __name__ == "__main__":
    main()
