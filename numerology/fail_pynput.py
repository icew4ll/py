"""test"""

from pynput import keyboard


def test():
    """test"""
    print("Press s or n to continue:")

    with keyboard.Events() as events:
        # Block for as much as possible
        event = events.get(1e6)
        print(f"{event.key}")
        if event.key == keyboard.KeyCode.from_char("s"):
            print("YES")
        else:
            print("NO")


def main():
    """test"""
    while 1:
        with keyboard.Events() as events:
            # Block for as much as possible
            print(f"{events.get(1e6).key}")


if __name__ == "__main__":
    main()
