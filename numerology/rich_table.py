"""chart"""
from dataclasses import dataclass
from typing import Tuple
from rich.console import Console
from rich.table import Table


@dataclass(frozen=True)
class Chart:
    """table"""

    rows: Tuple[Tuple[str, ...], ...] = (
        # (1, 2, 3, 4, 5, ", "g", "h", "i"),
        ("A", "B", "C", "D", "E", "F", "G", "H", "I"),
        ("J", "K", "L", "M", "N", "O", "P", "Q", "R"),
        ("S", "T", "U", "V", "W", "X", "Y", "Z", ""),
    )

    header: Tuple[int, ...] = tuple(i for i in range(1, 10))

    def render(self) -> None:
        """render"""
        # setup
        console = Console()
        table = Table(show_header=True, header_style="bold magenta")

        # header
        row_1 = next(i for i in self.rows)
        for i in enumerate(row_1):
            table.add_column(str(i[0] + 1))

        # rows
        rows = (i for i in self.rows)
        for row in rows:
            table.add_row(
                row[0],
                row[1],
                row[2],
                row[3],
                row[4],
                row[5],
                row[6],
                row[7],
                row[8],
            )

        console.print(table)


def main():
    """main"""
    char = Chart()
    char.render()


if __name__ == "__main__":
    main()
