import os
import shutil
from subprocess import run

import typer

app = typer.Typer()

home = os.environ["HOME"]
bin_dir = f"{home}/bin"
config = f"{home}/.config"
dot_url = "https://gitlab.com/users/icew4ll/public"
dot_dir = f"{home}/m/public"


def giter(cmds):
    repos = [
        f"{home}/m/public",
        f"{home}/m/dot",
        f"{home}/m/py",
        f"{home}/m/vim",
        f"{home}/m/gats",
        f"{home}/m/rust",
        f"{home}/m/vlang",
        f"{home}/m/nerd",
    ]

    for i in repos:
        typer.secho(f"REPO: {i}", fg=typer.colors.GREEN)
        os.chdir(i)
        cmder(cmds)


def find_files(dir, exclude=[]):
    """find files"""
    for item in os.scandir(dir):
        # if item.is_file():
        if item.is_dir():
            yield from find_files(item.path, exclude)
        elif item.is_file() and not any(i in item.path for i in exclude):
            yield item.path
        else:
            pass


def linker(src: str, dest):
    """soft links src to dest"""
    typer.secho(f"Linking {src} to {dest}", fg=typer.colors.YELLOW)
    if os.path.isfile(dest):
        os.unlink(dest)
    os.symlink(src, dest)


def dir_reset(dir: str):
    """temp directory reset"""
    # remove temp dir if exists
    if os.path.isdir(dir):
        print("removing ", dir)
        shutil.rmtree(dir)


def suber(cmd):
    typer.secho(f"Running {cmd}", fg=typer.colors.YELLOW)
    run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)


def cmder(cmds):
    """iterate list of commands"""
    for cmd in cmds:
        suber(cmd)


@app.command()
def python(name: str):
    typer.secho(f"hi {name}", fg=typer.colors.GREEN)


@app.command()
def preqs():
    """install dependencies"""
    rofi = [
        "check",
        "libgdk-pixbuf2.0-dev",
        "libstartup-notification0-dev",
        "libpango1.0-dev",
        "libxkbcommon-x11-dev",
        "libxcb-xinerama0-dev",
        "libxcb-xrm-dev",
        "libxcb-icccm4-dev",
        "libxcb-ewmh-dev",
        "libxcb-util-dev",
        "libxcb-xkb-dev",
        "libevent-dev",
        "libgtk2.0-dev",
        "libxcb-randr0-dev",
    ]
    nnn = [
        "libncurses-dev",
        "build-essential",
        "bison",
        "flex",
        "pkg-config",
        "libncursesw5-dev",
        "libreadline-dev",
    ]
    alacritty = [
        "cmake",
        "pkg-config",
        "libfreetype6-dev",
        "libfontconfig1-dev",
        "libxcb-xfixes0-dev",
        "python3",
    ]
    basic = [
        "jq",
        "libssl-dev",
        "imagemagick",
        "ubuntu-gnome-desktop",
        "sysstat",
        "nmap",
        "proxychains4",
        "rdesktop",
        "nmap",
        "clang",
        "arandr",
        "pavucontrol",
        "xclip",
        "vlc",
        "ffmpeg",
        "shellcheck",
        "expect",
        "cmus",
        "build-essential",
        "apt-file",
        "curl",
        "git",
        "wget",
        "firefox",
        "thunderbird",
        "zsh",
        "clang",
        "fonts-noto-color-emoji",
    ]
    nvim = [
        "ninja-build",
        "gettext",
        "libtool",
        "libtool-bin",
        "autoconf",
        "automake",
        "cmake",
        "g++",
        "pkg-config",
        "unzip",
    ]
    dwm = [
        "libx11-dev",
        "libxft-dev",
        "libxinerama-dev",
        "xorg",
        "xdm",
        "suckless-tools",
        "dmenu",
    ]
    ctags = [
        "gcc",
        "make",
        "pkg-config",
        "autoconf",
        "automake",
        "python3-docutils",
        "libseccomp-dev",
        "libjansson-dev",
        "libyaml-dev",
        "libxml2-dev",
    ]
    py = ["zlib1g-dev"]
    all = " ".join(rofi + nnn + alacritty + dwm + basic + ctags + nvim + py)
    cmds = [
        "sudo apt update && sudo apt upgrade",
        f"sudo apt install {all}",
    ]
    cmder(cmds)


def get_name_dir(url):
    name = os.path.basename(url)
    dir = f"/tmp/{name}"
    return (name, dir)


@app.command()
def ran():
    url = "https://github.com/rust-analyzer/rust-analyzer"
    name, dir = get_name_dir(url)
    cmds = (
        "rustup component add rust-src",
        f"git clone {url} {dir}",
        f"( cd {dir} && cargo xtask install )",
    )
    cmder(cmds)


@app.command()
def slock():
    url = "git://git.suckless.org/slock"
    name = os.path.basename(url)
    dir = f"/tmp/{name}"
    dir_reset(dir)
    cmds = (
        f"git clone {url} {dir}",
        f"sudo make -C {dir} clean install",
    )
    cmder(cmds)


@app.command()
def dwm():
    url = "git://git.suckless.org/dwm"
    name = os.path.basename(url)
    dir = f"/tmp/{name}"
    file = f"{dir}/config.def.h"
    dir_reset(dir)
    cmds = (
        f"git clone {url} {dir}",
        f"sed -ie '/MODKEY Mod1Mask/s/Mod1Mask/Mod4Mask/' {file}",
        f'sed -ie \'/"st"/s/"st"/"rofi", "-show", "drun", "-show-icons"/\' {file}',
        f"sed -ie '/monospace:size=10/s/monospace:size=10/JetBrainsMono Nerd Font Mono:size=16/' {file}",
        f"sudo make -C {dir} clean install",
    )
    cmder(cmds)


@app.command()
def rofi():
    """rofi install"""
    url = "https://github.com/DaveDavenport/rofi"
    name = os.path.basename(url)
    dir = f"{bin_dir}/build/{name}"
    dir_reset(dir)
    # git clone
    cmd = f"git clone --recursive {url} {dir}"
    suber(cmd)
    os.chdir(dir)
    cmds = [
        "git pull && git submodule update --init && autoreconf -i",
        "mkdir build",
        "./configure",
        "make",
        "sudo make install",
    ]
    cmder(cmds)


@app.command()
def rust():
    if shutil.which("rustc"):
        typer.secho("RUST INSTALLED", fg=typer.colors.GREEN)
        pkgs = [
            "grex",
            "gifski",
            "funzzy",
            "caretaker",
            "pier",
            "rargs",
            "cargo-incversion",
            "procs",
            "cargo-update",
            "cargo-edit",
            "cargo-outdated",
            "skim",
            "git-delta",
            "bat",
            "lsd",
            "zoxide",
            "fd-find",
            "ripgrep",
            "watchexec",
            "bb",
            "bottom",
            "shotgun",
            "xcolor",
            "gitui",
            "sfz",
            "stork-search",
            "t-rec",
        ]
        components = [
            "clippy",
            "rust-src",
            "rustfmt",
        ]
        git = ["https://github.com/skallwar/suckit"]
        cmds = [
            "rustup update",
            f'rustup component add {" ".join(components)}',
            f'cargo install {" ".join(pkgs)}',
            f'cargo install --git {" ".join(git)}',
            "cargo install-update -a",
        ]
        cmder(cmds)
    else:
        print("rust not installed")
        suber("curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh")


@app.command()
def gifsicle():
    url = "https://github.com/kohler/gifsicle"
    dir = f"/tmp/{os.path.basename(url)}"
    dir_reset(dir)
    # git clone
    cmd = f"git clone {url} {dir}"
    suber(cmd)
    # make
    dest = f"{bin_dir}/build/{name}"
    # dest = "/tmp/test"
    release = "CMAKE_BUILD_TYPE=Release"
    prefix = f"CMAKE_INSTALL_PREFIX={dest}"
    cmd = f"make {release} {prefix} install"
    dir_reset(dest)
    os.chdir(dir)
    suber(cmd)
    src = f"{dest}/bin/{name}"
    dest = f"{bin_dir}/{name}"
    linker(src, dest)
    typer.secho("NVIM INSTALL COMPLETE", fg=typer.colors.GREEN)


@app.command()
def nvim():
    name = "nvim"
    dir = "/tmp/neovim"
    url = "https://github.com/neovim/neovim"
    dir_reset(dir)
    # git clone
    cmd = f"git clone {url} {dir}"
    suber(cmd)
    # make
    dest = f"{bin_dir}/build/{name}"
    # dest = "/tmp/test"
    release = "CMAKE_BUILD_TYPE=Release"
    prefix = f"CMAKE_INSTALL_PREFIX={dest}"
    cmd = f"make {release} {prefix} install"
    dir_reset(dest)
    os.chdir(dir)
    suber(cmd)
    src = f"{dest}/bin/{name}"
    dest = f"{bin_dir}/{name}"
    linker(src, dest)
    typer.secho("NVIM INSTALL COMPLETE", fg=typer.colors.GREEN)


@app.command()
def load():
    """download dots"""
    cmds = ("git pull",)

    def cp_to():
        def backer(file):
            dest = f'{home}/{file.split("dots/")[1]}'
            print(f"{file} --> {dest}")
            dir = os.path.dirname(dest)
            if os.path.isdir(dir) is False:
                os.makedirs(dir)
            shutil.copy(file, dest)

        for i in find_files(f"{dot_dir}/dots"):
            backer(i)

    giter(cmds)
    cp_to()


@app.command()
def save():
    """upload dots"""
    public = f"{dot_dir}/dots"
    configs = (
        "/opt/ktrl/cfg.ron",
        f"{home}/.zshrc",
        f"{home}/.p10k.zsh",
        f"{home}/.tmux.conf.local",
        f"{config}/alacritty/alacritty.yml",
    )
    files = (
        (
            f"{config}/nvim",
            [
                "/tmp/",
                "/src/",
                "/.git/",
                "/plugin/",
                "/pack/",
                "nvimlog",
            ],
        ),
        (f"{config}/efm-langserver", []),
        (f"{config}/zsh", []),
        (f"{config}/rofi", []),
    )
    cmds = (
        "git add -A",
        "git commit -m 'up dots'",
        "git push",
    )

    def saver():
        """save dots"""

        def backer(file):
            dest = f'{public}{os.path.dirname(file).replace(home, "")}/{os.path.basename(file)}'
            print(f"{file} --> {dest}")
            dir = os.path.dirname(dest)
            if os.path.isdir(dir) is False:
                os.makedirs(dir)
            shutil.copy(file, dest)

        typer.secho("SAVING...", fg=typer.colors.GREEN)

        for dir, exclude in files:
            for i in find_files(dir, exclude):
                backer(i)

        for i in configs:
            backer(i)

    dir_reset(public)
    saver()
    giter(cmds)


if __name__ == "__main__":
    app()
