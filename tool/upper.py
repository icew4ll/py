import io
import os
import re
import shutil
import tarfile
import urllib.request
from pathlib import Path
from shutil import copy2
from subprocess import run

from bs4 import BeautifulSoup
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter
from requests import get


class build:
    home = os.environ["HOME"]
    bin_dir = f"{home}/bin"
    config = f"{home}/.config"
    dot_url = "https://gitlab.com/users/icew4ll/public"
    dot_dir = f"{home}/m/public"

    @staticmethod
    def find_files(dir, exclude=[]):
        """find files"""
        for item in os.scandir(dir):
            # if item.is_file():
            if item.is_dir():
                yield from build().find_files(item.path, exclude)
            elif item.is_file() and not any(i in item.path for i in exclude):
                yield item.path
            else:
                pass

    @staticmethod
    def get_html(url):
        res = get(url)
        return res.text

    @staticmethod
    def suber(cmd):
        run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)

    @staticmethod
    def cmder(cmds):
        """iterate list of commands"""
        print(cmds)
        for cmd in cmds:
            print("Running: ", cmd)
            build().suber(cmd)

    @staticmethod
    def is_tool(name):
        """Check whether `name` is on PATH and marked as executable."""
        return shutil.which(name) is not None

    @staticmethod
    def dependencies():
        """install dependencies"""
        rofi = [
            "check",
            "libgdk-pixbuf2.0-dev",
            "libstartup-notification0-dev",
            "libpango1.0-dev",
            "libxkbcommon-x11-dev",
            "libxcb-xinerama0-dev",
            "libxcb-xrm-dev",
            "libxcb-icccm4-dev",
            "libxcb-ewmh-dev",
            "libxcb-util-dev",
            "libxcb-xkb-dev",
            "libevent-dev",
            "libgtk2.0-dev",
            "libxcb-randr0-dev",
        ]
        nnn = [
            "libncurses-dev",
            "build-essential",
            "bison",
            "flex",
            "pkg-config",
            "libncursesw5-dev",
            "libreadline-dev",
        ]
        alacritty = [
            "cmake",
            "pkg-config",
            "libfreetype6-dev",
            "libfontconfig1-dev",
            "libxcb-xfixes0-dev",
            "python3",
        ]
        basic = [
            "clang",
            "arandr",
            "pavucontrol",
            "xclip",
            "vlc",
            "ffmpeg",
            "shellcheck",
            "expect",
            "cmus",
            "build-essential",
            "apt-file",
            "curl",
            "git",
            "wget",
            "firefox",
            "thunderbird",
            "zsh",
            "clang",
            "fonts-noto-color-emoji",
        ]
        nvim = [
            "ninja-build",
            "gettext",
            "libtool",
            "libtool-bin",
            "autoconf",
            "automake",
            "cmake",
            "g++",
            "pkg-config",
            "unzip",
        ]
        dwm = [
            "libx11-dev",
            "libxft-dev",
            "libxinerama-dev",
            "xorg",
            "xdm",
            "suckless-tools",
            "dmenu",
        ]
        ctags = [
            "gcc",
            "make",
            "pkg-config",
            "autoconf",
            "automake",
            "python3-docutils",
            "libseccomp-dev",
            "libjansson-dev",
            "libyaml-dev",
            "libxml2-dev",
        ]
        py = [
            "zlib1g-dev"
        ]
        all = " ".join(rofi + nnn + alacritty + dwm + basic + ctags + nvim + py)
        cmds = [
            "sudo apt update && sudo apt upgrade",
            f"sudo apt install {all}",
            "sudo apt-file update",
        ]
        for cmd in cmds:
            build().suber(cmd)
        if shutil.which("rustc"):
            print("rust installed")
            cmds = "cargo install git-delta bat lsd zoxide fd-find ripgrep watchexec bb"
            build().suber(cmd)
        else:
            build().suber(
                "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh"
            )

    @staticmethod
    def get_links(url):
        html = build().get_html(url)
        soup = BeautifulSoup(html, "html.parser")
        href = soup.find_all("a", href=True)
        return (a.get("href") for a in href)

    @staticmethod
    def dl_tar_gz(dl, dest):
        print(f"downloading: {dl}, to {dest}")
        byte = urllib.request.urlopen(dl)
        io_bytes = io.BytesIO(byte.read())
        tar = tarfile.open(fileobj=io_bytes, mode="r|gz")
        tar.extractall(dest)
        tar.close()

    @staticmethod
    def linker(src, dest):
        """soft links src to dest"""
        print(f"Linking {src} to {dest}")
        if os.path.isfile(dest):
            os.unlink(dest)
        os.symlink(src, dest)

    @staticmethod
    def unlinker(dir):
        """soft links src to dest"""
        print(f"Unlinking {dir}")
        if os.path.isfile(dir):
            os.unlink(dir)

    @staticmethod
    def dir_reset(dir):
        """temp directory reset"""
        # remove temp dir if exists
        if os.path.isdir(dir):
            print("removing ", dir)
            shutil.rmtree(dir)

    @staticmethod
    def luarocks():
        """install luarocks"""
        name = "luarocks"
        url = "https://luarocks.github.io/luarocks/releases"
        tag = ".tar.gz"
        links = build().get_links(url)

        def get_dl(links):
            """get dl from links"""
            # luarocks-3.5.0.tar.gz
            r = re.compile("luarocks-[0-9].[0-9].[0-9].tar.gz$")
            for i in links:
                # print(i)
                if r.match(i):
                    return i

        def downer():
            # download python to /tmp
            prefix = "https://luarocks.github.io/luarocks/releases/"
            dl = f"{prefix}{get_dl(links)}"
            dest = "/tmp"
            base_name = os.path.basename(dl).replace(f"{tag}", "")
            dl_dir = f"{dest}/{base_name}"
            build().dir_reset(dl_dir)
            build().dl_tar_gz(dl, dest)
            # build
            print(f"building in {dl_dir}...")
            build_dir = f'{os.environ["HOME"]}/bin/build/{name}'
            build().dir_reset(build_dir)
            loc = f"--prefix={build_dir}"
            # opt = "--with-lua={build().bin_dir}/lua"
            cmds = [
                f"./configure {loc}",
                "make",
                "make install",
            ]
            os.chdir(dl_dir)
            build().cmder(cmds)
            src = f"{build_dir}/bin/{name}"
            dest = f"{build().bin_dir}/{name}"
            build().unlinker(dest)
            build().linker(src, dest)

        downer()

    @staticmethod
    def lua():
        """update python"""
        print("upping python...")
        name = "lua"
        url = "http://www.lua.org/ftp"
        tag = "tar.gz"
        links = build().get_links(url)

        def get_dl(links):
            """get dl from links"""
            r = re.compile("lua-[0-9].[0-9].[0-9].tar.gz$")
            # return next(x.path for x in os.scandir(dir) if r.match(x.path))
            for i in links:
                if r.match(i):
                    return i

        def downer():
            # download python to /tmp
            # http://www.lua.org/ftp/lua-5.4.2.tar.gz
            dl = f"http://www.lua.org/ftp/{get_dl(links)}"
            dest = "/tmp"
            base_name = os.path.basename(dl).replace(f".{tag}", "")
            dir = f"{dest}/{base_name}"
            print("removing ", dir)
            build().dir_reset(dir)
            build().dl_tar_gz(dl, dest)
            # build
            print(f"building in {dir}...")
            build_dir = f'{os.environ["HOME"]}/bin/build/{name}'
            build().dir_reset(build_dir)
            cmds = [
                "make linux test",
                # "make install",
                f"make INSTALL_TOP={build_dir} install",
            ]
            os.chdir(dir)
            build().cmder(cmds)
            src = f"{build_dir}/bin/{name}"
            dest = f"{build().bin_dir}/{name}"
            build().unlinker(dest)
            build().linker(src, dest)

        downer()

    @staticmethod
    def python_update():
        """update python"""
        print("upping python...")
        url = "https://www.python.org/downloads/source/"
        tag = "tgz"
        links = build().get_links(url)

        def get_dl(links):
            """get dl from links"""
            for i in links:
                if tag in i:
                    return i

        def find_python(dir):
            try:
                r = re.compile(".*python[0-9].[0-9]$")
                return next(x.path for x in os.scandir(dir) if r.match(x.path))
            except Exception:
                print("pythonX.X not found!")

        def downer():
            # download python to /tmp
            dl = get_dl(links)
            dest = "/tmp"
            base_name = os.path.basename(dl).replace(f".{tag}", "")
            dir = f"{dest}/{base_name}"
            # print("removing ", dir)
            # build().dir_reset(dir)
            build().dl_tar_gz(dl, dest)
            # build
            print(f"building in {dir}...")
            build_dir = f'{os.environ["HOME"]}/bin/build/python'
            if os.path.isdir(build_dir):
                shutil.rmtree(build_dir)
            ssl = "--with-openssl=/usr/bin/openssl"
            opt = f"--prefix={build_dir} --enable-optimizations {ssl}"
            cmds = [
                f"./configure {opt}",
                "make install",
            ]
            os.chdir(dir)
            build().cmder(cmds)
            py_bin = find_python(f"{build_dir}/bin")
            dest = f'{os.environ["HOME"]}/bin/{os.path.basename(py_bin)}'
            build().linker(py_bin, dest)

        downer()

    @staticmethod
    def go_lang():
        print("upping go...")
        url = "https://golang.org/dl/"
        links = build().get_links(url)

        def get_dl(links):
            """get dl from links"""
            tag = "linux-amd64.tar.gz"
            for i in links:
                if tag in i:
                    return i.replace("/dl/", "")
                    break

        def dl_file():
            """dl file"""
            dest = f'{os.environ["HOME"]}/bin/build'
            bin = f'{os.environ["HOME"]}/bin/go'
            print(dest)
            file = get_dl(links)
            dl = f"{url}{file}"
            if os.path.isdir(f"{dest}/go"):
                shutil.rmtree(f"{dest}/go")
            byte = urllib.request.urlopen(dl)
            io_bytes = io.BytesIO(byte.read())
            tar = tarfile.open(fileobj=io_bytes, mode="r|gz")
            tar.extractall(dest)
            tar.close()
            src = f"{dest}/go/bin/go"
            build.linker(src, bin)

        dl_file()
        print("\nGO UP COMPLETE")

    @staticmethod
    def tmux():
        """install tmux"""
        url = "https://github.com/tmux/tmux/releases"
        links = build().get_links(url)

        def get_dl(links):
            """get dl from links"""
            tag = "tar.gz"
            for i in links:
                if tag in i:
                    return i
                    break

        def dl_tar_gz(dl, dest):
            byte = urllib.request.urlopen(dl)
            io_bytes = io.BytesIO(byte.read())
            tar = tarfile.open(fileobj=io_bytes, mode="r|gz")
            tar.extractall(dest)
            tar.close()

        def build_tmux():
            dl = f"https://github.com{get_dl(links)}"
            print(dl)
            dest = "/tmp"
            dir = f'{dest}/{os.path.basename(dl).replace(".tar.gz", "")}'
            print(dir)

            # remove temp dir if exists
            if os.path.isdir(dir):
                shutil.rmtree(dir)

            dl_tar_gz(dl, dest)
            os.chdir(dir)
            cmd = f'./configure --prefix={os.environ["HOME"]} && make install'
            build().suber(cmd)

        build_tmux()

    @staticmethod
    def alacritty():
        """install alacritty"""
        dir = "/tmp/alacritty"
        url = "https://github.com/alacritty/alacritty"
        # remove temp dir if exists
        if os.path.isdir(dir):
            shutil.rmtree(dir)
        # git clone
        cmd = f"git clone {url} {dir}"
        build().suber(cmd)
        # make
        os.chdir(dir)
        cmds = [
            "cargo build --release",
            "sudo tic -xe alacritty,alacritty-direct extra/alacritty.info",
            "sudo cp target/release/alacritty /usr/local/bin",
            "sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg",
            "sudo desktop-file-install extra/linux/Alacritty.desktop",
            "sudo update-desktop-database",
        ]
        for cmd in cmds:
            build().suber(cmd)

    # latest git clone?
    @staticmethod
    def ctags():
        """ctag install"""
        dir = "/tmp/ctags"
        url = "https://github.com/universal-ctags/ctags"
        build().dir_reset(dir)
        # git clone
        cmd = f"git clone {url} {dir}"
        build().suber(cmd)
        # make
        # print(build().bin_dir)
        # dest = "/tmp"
        dest = f"{build().bin_dir}/build/ctags"
        os.chdir(dir)
        cmds = ["./autogen.sh", f"./configure --prefix={dest}", "make install"]
        build().dir_reset(dest)
        build().cmder(cmds)
        src = f"{dest}/bin/ctags"
        dest = f"{build().bin_dir}/ctags"
        build().unlinker(f"{dest}/ctags")
        build().linker(src, dest)

    @staticmethod
    def rofi():
        """ctag install"""
        name = "rofi"
        dir = f"{build().bin_dir}/build/{name}"
        url = "https://github.com/DaveDavenport/rofi"
        build().dir_reset(dir)
        # git clone
        cmd = f"git clone --recursive {url} {dir}"
        build().suber(cmd)
        os.chdir(dir)
        cmds = [
            "git pull && git submodule update --init && autoreconf -i",
            "mkdir build",
            "./configure",
            "make",
        ]
        build().cmder(cmds)
        src = f"{dir}/{name}"
        dest = f"{build().bin_dir}/{name}"
        build().linker(src, dest)

    @staticmethod
    def nvim():
        name = "nvim"
        dir = "/tmp/neovim"
        url = "https://github.com/neovim/neovim"
        build().dir_reset(dir)
        # git clone
        cmd = f"git clone {url} {dir}"
        build().suber(cmd)
        # make
        dest = f"{build().bin_dir}/build/{name}"
        # dest = "/tmp/test"
        release = "CMAKE_BUILD_TYPE=Release"
        prefix = f"CMAKE_INSTALL_PREFIX={dest}"
        cmd = f"make {release} {prefix} install"
        build().dir_reset(dest)
        os.chdir(dir)
        build().suber(cmd)
        src = f"{dest}/bin/{name}"
        dest = f"{build().bin_dir}/{name}"
        build().unlinker(f"{dest}/{name}")
        build().linker(src, dest)

    @staticmethod
    def nnn():
        """update nnn"""
        dir = "/tmp/nnn"
        url = "https://github.com/jarun/nnn"
        # remove temp dir if exists
        if os.path.isdir(dir):
            shutil.rmtree(dir)
        # git clone
        cmd = f"git clone {url} {dir}"
        build().suber(cmd)
        # make
        options = "O_NERD=1"
        cmd = f"make {options}"
        os.chdir(dir)
        build().suber(cmd)
        # move bin
        dest = f'{os.environ["HOME"]}/bin/nnn'
        src = f"{dir}/nnn"
        if os.path.isfile(dest):
            os.remove(dest)
        shutil.move(src, dest)
        # copy plugin directory
        src = f"{dir}/plugins"
        loc = f"{build().config}/nnn"
        plug = f"{loc}/plugins"
        if os.path.isdir(plug) is False:
            os.makedirs(plug)
        for file in os.scandir(src):
            src = file.path
            dest = f"{plug}/{os.path.basename(src)}"
            shutil.move(src, dest)
        print("NNN INSTALL COMPLETE")

    @staticmethod
    def save():
        """upload dots"""
        home = build().home
        config = build().config
        public = f"{build().dot_dir}/dots"
        zshrc = f"{home}/.zshrc"
        tenk = f"{home}/.p10k.zsh"
        tmux = f"{home}/.tmux.conf.local"
        alacritty = f"{config}/alacritty/alacritty.yml"
        zsh = f"{config}/zsh"
        nvim = f"{config}/nvim"
        efm = f"{config}/efm-langserver"
        rofi = f"{config}/rofi"
        ktrl = "/opt/ktrl/cfg.ron"

        # if os.path.isdir(public) is False:
        #     cmd = f"git clone {build().dot_url} {build().dot_dir}"

        def backer(file):
            dest = f'{public}{os.path.dirname(file).replace(home, "")}/{os.path.basename(file)}'
            print(f"{file} --> {dest}")
            dir = os.path.dirname(dest)
            if os.path.isdir(dir) is False:
                os.makedirs(dir)
            shutil.copy(file, dest)

        def cp_nvim():
            """copy nvim"""
            exclude = [
                "/tmp/",
                "/src/",
                "/.git/",
                "/plugin/",
                "/pack/",
                "nvimlog",
            ]
            for i in build().find_files(nvim, exclude):
                backer(i)

        def cp_zsh():
            """copy nvim"""
            for i in build().find_files(zsh):
                backer(i)
            backer(zshrc)

        def cp_efm():
            """copy nvim"""
            for i in build().find_files(efm):
                backer(i)

        def cp_rofi():
            """copy nvim"""
            for i in build().find_files(rofi):
                backer(i)

        def git_push():
            home = build().home
            repos = [
                f"{home}/m/public",
                f"{home}/m/dot",
                f"{home}/m/py",
                f"{home}/m/vim",
                f"{home}/m/gats",
            ]
            cmds = [
                "git add -A",
                "git commit -m 'up dots'",
                "git push",
            ]

            cp_nvim()
            cp_zsh()
            cp_efm()
            cp_rofi()
            backer(alacritty)
            backer(tenk)
            backer(tmux)
            backer(ktrl)
            backer(zshrc)
            for i in repos:
                os.chdir(i)
                build().cmder(cmds)

        git_push()

    @staticmethod
    def load():
        """upload dots"""
        home = build().home
        public = f"{build().dot_dir}/dots"

        # if os.path.isdir(public) is False:
        #     cmd = f"git clone {build().dot_url} {build().dot_dir}"

        def cp_to():
            def backer(file):
                dest = f'{home}/{file.split("dots/")[1]}'
                print(f"{file} --> {dest}")
                dir = os.path.dirname(dest)
                if os.path.isdir(dir) is False:
                    os.makedirs(dir)
                shutil.copy(file, dest)

            for i in build().find_files(public):
                backer(i)

        def git_pull():
            home = build().home
            repos = [
                f"{home}/m/public",
                f"{home}/m/dot",
                f"{home}/m/py",
                f"{home}/m/vim",
                f"{home}/m/gats",
            ]
            cmds = [
                "git pull",
            ]

            for i in repos:
                os.chdir(i)
                build().cmder(cmds)

            cp_to()

        git_pull()

    @staticmethod
    def zinit():
        """upload dots"""
        cmd = 'sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"'
        build().suber(cmd)
        pass

    def dwm(self):
        url = "git://git.suckless.org/dwm"
        name = os.path.basename(url)
        dir = f"/tmp/{name}"
        file = f"{dir}/config.def.h"
        self.dir_reset(dir)
        cmds = (
            f"git clone {url} {dir}",
            f"sed -ie '/MODKEY Mod1Mask/s/Mod1Mask/Mod4Mask/' {file}",
            f'sed -ie \'/"st"/s/"st"/"rofi", "-show", "drun", "-show-icons"/\' {file}',
            f"sed -ie '/monospace:size=10/s/monospace:size=10/JetBrainsMono Nerd Font Mono:size=14/' {file}",
            f"sudo make -C {dir} clean install",
            # f"(cd {dir} && sudo make clean install)",
        )
        self.cmder(cmds)


if __name__ == "__main__":
    print("\nRESET")
    # build().up_go()

    # remove util functions
    s1 = set(dir(build()))
    s2 = {
        "suber",
        "get_links",
        "get_html",
        "dir_reset",
        "linker",
        "unlinker",
        "is_tool",
        "dl_tar_gz",
        "cmder",
        "bin_dir",
        "dot_dir",
        "dot_url",
    }
    cmds = [i for i in s1 - s2 if "__" not in i]
    cmd_completer = WordCompleter(cmds)
    while True:
        text = prompt(
            "Input commands: ",
            completer=cmd_completer,
            complete_while_typing=False,
        )
        print(f"You said: {text}")
        # instantiate class and run method
        # b = build()
        res = getattr(build(), text)()
