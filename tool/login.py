import yaml
import json
import os


class Servers:
    def __init__(self):
        with open(os.environ["data"]) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        self.data = data["servers"]


if __name__ == "__main__":
    print("=" * 79)
    Servers().miko("racktables")
