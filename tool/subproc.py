import os
import shutil
from subprocess import check_output, run

from sudo import run_as_sudo


def suber(n):
    # out = run(n, shell=True, executable="/bin/bash", universal_newlines=True)
    out = run(n, shell=True, executable="/bin/bash", universal_newlines=True)
    print(out)


def cloner():
    """"""
    # dir = "/tmp/neovim"
    # if os.path.isdir(dir):
    #     print("ok")
    # cmds = """
    # git clone https://github.com/neovim/neovim && cd neovim
    # sudo make CMAKE_BUILD_TYPE=Release install
    # """
    dir = "/tmp/neovim"
    if os.path.isdir(dir):
        shutil.rmtree(dir)

    url = "https://github.com/neovim/neovim"
    cmd = f"git clone {url} {dir}"
    # run(['sudo', 'bash', '-c', cmd])
    run(["bash", "-c", cmd])
    cmd = "make CMAKE_BUILD_TYPE=Release CMAKE_INSTALL_PREFIX=$HOME/bin/build/nvim install"
    os.chdir(dir)
    run([cmd], executable="/bin/bash", shell=True)
    cmd = "ln -s $HOME/bin/build/nvim/bin/nvim $HOME/bin/nvim"
    run([cmd], executable="/bin/bash", shell=True)
    print("COMPLETE")


# r = run(["ls"], capture_output=True, encoding="utf-8").stdout
# r = run(["git", "clone", url, dir], capture_output=True, encoding="utf-8").stdout
# print(r)
# cmd = "make CMAKE_BUILD_TYPE=Release install"
# subprocess.run(['sudo', 'bash', '-c', bashCommand])
# cmd = "make CMAKE_BUILD_TYPE=Release install"
# sudo_user = "root"
# # run_as_sudo(sudo_user, cmd, shell=True, timeout=20)
# run_as_sudo(sudo_user, cmd)

# dir = "/tmp"
# # dir = "/tmp"
# os.chdir(dir)
# # r = subprocess.run(["ls", "-l"], capture_output=True, encoding="utf-8").stdout
# url = "https://github.com/neovim/neovim"
# r = subprocess.check_output(["git", "clone", url], encoding="utf-8")
# print(r)
# os.chdir(dir)
# r = subprocess.check_output(
#     ["sudo", "make", "CMAKE_BUILD_TYPE=Release," "install"], encoding="utf-8"
# )
# print(r)
# commands = '''
# echo "a"
# echo "b"
# echo "c"
# echo "d"
# '''

# process = subprocess.Popen('/bin/bash', stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# out, err = process.communicate(commands)
# print out

if __name__ == "__main__":
    cloner()
