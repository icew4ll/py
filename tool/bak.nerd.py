import json
import os
import shutil
from itertools import chain
from subprocess import run

import typer
import yaml
from typer import colors, secho

app = typer.Typer()


class Data:

    home = os.environ["HOME"]
    bin_dir = f"{home}/bin"
    home_dir = os.environ["HOME"]
    config_dir = f'{os.environ["HOME"]}/.config'
    user = os.environ["USER"]

    def __init__(self):
        file = "nerd.yml"
        with open(file, "r") as stream:
            try:
                data = yaml.safe_load(stream)
                # print(json.dumps(data, indent=1))
                self.d = data
            except yaml.YAMLError as e:
                print(e)

    def use(self):
        print(json.dumps(self.d, indent=1))

    @staticmethod
    def suber(cmd: str):
        typer.secho(f"RUNNING:\n{cmd}", fg=typer.colors.YELLOW)
        run(cmd, executable="/bin/bash", encoding="utf-8", shell=True)

    @staticmethod
    def linker(src: str, dest):
        typer.secho(f"Linking {src} to {dest}", fg=typer.colors.YELLOW)
        if os.path.islink(dest):
            os.unlink(dest)
        os.symlink(src, dest)

    @staticmethod
    def dir_reset(dir: str):
        """temp directory reset"""
        if os.path.isdir(dir):
            secho(f"CLEANING: {dir}", fg=colors.YELLOW)
            shutil.rmtree(dir)

    def cmder(self, cmds: str):
        for cmd in cmds:
            self.suber(cmd)

    def find_files(self, dir, exclude):
        """find files"""
        for item in os.scandir(dir):
            # if item.is_file():
            if item.is_dir():
                yield from self.find_files(item.path, exclude)
            elif item.is_file() and not any(i in item.path for i in exclude):
                yield item.path
            else:
                pass

    def save(self):
        # secho(json.dumps(self.d, indent=1), fg=colors.BLUE)
        sync = self.d["sync"]
        # paths
        paths = sync["paths"]
        root = paths["root"]
        dot = paths["dot"]
        # repos
        repos = sync["repos"]
        # configs
        configs = sync["configs"]
        files = configs["files"]
        system, home, config = (files["system"], files["home"], files["config"])
        flat_files = chain(
            system,
            [f"{self.home_dir}/{i}" for i in home],
            [f"{self.config_dir}/{i}" for i in config],
        )
        dirs = configs["dirs"]

        def giter(cmds):
            for i in repos:
                repo = f"{self.home_dir}/{root}/{i}"
                typer.secho(f"REPO: {repo}", fg=typer.colors.GREEN)
                os.chdir(repo)
                self.cmder(cmds)

        cmds = (
            "git add -A",
            "git commit -m 'up dots'",
            "git push",
        )

        def file_backer(file):
            file_rep = f"{file}".replace(f"/{self.user}", "")
            dest = f"{self.home_dir}/{root}/{dot}{file_rep}"
            cmd = f"{file} => {dest}"
            secho(cmd, fg=colors.BLUE)
            dir = os.path.dirname(dest)
            if os.path.isdir(dir) is False:
                os.makedirs(dir)
            shutil.copy(file, dest)

        def dir_backer():
            def files(key, path):
                for i in dirs[key]:
                    name = i["name"]
                    dir = f"{path}/{name}"
                    exclude = i["exclude"]
                    secho(json.dumps((name, exclude), indent=1), fg=colors.BLUE)
                    yield self.find_files(dir, [] if exclude is None else exclude)

            for i in chain(files("config", self.config_dir), files("home", self.home_dir)):
                for x in i:
                    file_backer(x)

        try:
            dir_backer()
            for i in flat_files:
                file_backer(i)
            giter(cmds)

        except KeyError:
            pass

    def test(self):
        # secho(json.dumps(self.d, indent=1), fg=colors.BLUE)
        home_dir = os.environ["HOME"]
        config_dir = f'{os.environ["HOME"]}/.config'
        user = os.environ["USER"]
        sync = self.d["sync"]
        # paths
        paths = sync["paths"]
        root = paths["root"]
        dot = paths["dot"]
        try:
            dir_backer()
            for i in flat_files:
                file_backer(i)
            giter(cmds)

        except KeyError:
            pass

    def sys(self):
        # cleanse key errors
        sys = self.d["system"]
        update_cmds = sys["update"]
        apt, rust = (sys["apt"], sys["rust"])
        apt_cmd, rust_cmd = (apt["cmd"], rust["cmd"])
        apt_pkgs, rust_pkgs = ((i for i in apt["pkgs"]), (i for i in rust["pkgs"]))

        def repo_deps():
            key = "repos"
            repos = self.d[key]
            for i in repos.keys():
                # cleanse "system" none types
                sys = repos[i].get("system")
                if sys:
                    for i in sys:
                        yield i

        try:
            self.suber(
                f'{apt_cmd} {" ".join([i for i in chain(repo_deps(), apt_pkgs)])}'
            )
            self.suber(f'{" && ".join(i for i in update_cmds)}')
            self.suber(f'{rust_cmd} {" ".join([i for i in rust_pkgs])}')
        except KeyError:
            pass

    def install_repo(self, input: str):
        secho(json.dumps(self.d, indent=1), fg=colors.BLUE)
        key = "repos"
        try:
            repo = self.d[key][input]
            find = {"url", "cmds"}
            if (repo.keys()) >= find:
                print("All keys are present")
                cmds = " && ".join(repo.get("cmds"))
                url = repo.get("url")
                name = os.path.basename(url)
                dir = f"{self.home}/bin/build/{name}"
                bin = repo.get("bin")
                cmd = f"git clone {url} {dir} && cd {dir} && {cmds}"
                post = repo.get("post")
                self.dir_reset(dir)
                self.suber(cmd)
                if bin:
                    src = f"{dir}/{bin}"
                    dest = f"{self.bin_dir}/{os.path.basename(bin)}"
                    self.linker(src, dest)
                if post:
                    self.suber(post)
            else:
                print("All keys are not present")
        except KeyError:
            secho(f"Can't find keys: [{key}][{input}]", fg=colors.RED)


@app.command()
def repo(repository: str):
    d = Data()
    d.install_repo(repository)
    secho("COMPLETE", fg=colors.GREEN)


@app.command()
def sys():
    d = Data()
    d.sys()
    secho("COMPLETE", fg=colors.GREEN)


@app.command()
def save():
    d = Data()
    d.save()
    secho("COMPLETE", fg=colors.GREEN)


@app.command()
def test():
    d = Data()
    d.test()
    secho("COMPLETE", fg=colors.GREEN)


if __name__ == "__main__":
    secho("CONTINUOUS NERDIFICATION...", fg=colors.MAGENTA)
    app()
