#!/bin/bash

# bootstrap
# url="https://glcdn.rawgit.org/icew4ll/py/master/tool/install.sh"
# bash -c "$(curl -fsSL $url)"

preqs() {
	sudo apt update && sudo apt upgrade
	sudo apt install git python3-pip build-essential apt-file libx11-dev libxft-dev libxinerama-dev xorg xdm suckless-tools dmenu
}

dwm() {
	dir=$HOME/bin/build
	mkdir -p "$dir" && cd "$dir"
	git clone https://git.suckless.org/dwm
	sudo make clean install
	echo 'exec dwm' > ~/.xsession
}

py() {
	dir=~/m
	[ ! -d "$dir" ] && mkdir $dir
	dir=~/bin
	[ ! -d "$dir" ] && mkdir $dir
	src=https://gitlab.com/icew4ll/py
	dest=~/m/py
	git clone $src $dest
	py=$(command -v python3)
	$py -m pip install --user pipenv
	pip_env="python3 -m pipenv"
	(cd $dest && $pip_env install && $pip_env run python $dest/tool/upper.py)
}

preqs && dwm && py
