import json
import os
import sys

import yaml
from pexpect import pxssh

import pexpect

# import pexpect


class Data:
    def __init__(self, env):
        self.file = os.environ[env]

    def read_yaml(self):
        """open yaml"""

        with open(self.file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            # print(json.dumps(data, indent=1))
            print(json.dumps(data["servers"]["jb05"], indent=1))

    def login(self, name):
        with open(self.file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)

            ip = data["servers"][name]["ip"]
            user = data["servers"][name]["user"]
            pw = data["servers"][name]["pass"]
            try:
                s = pxssh.pxssh(
                    options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null",
                    },
                    encoding="utf-8",
                )
                s.logfile = sys.stdout
                s.login(ip, user, pw)
                s.sendline("hostnamectl")
                s.prompt()
                s.logout()
            except pxssh.ExceptionPxssh as e:
                print("pxssh failed on login.")
                print(e)

    def interact(self, name):
        with open(self.file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)

            ip = data["servers"][name]["ip"]
            user = data["servers"][name]["user"]
            pw = data["servers"][name]["pass"]
            try:
                ssh_newkey = "Are you sure you want to continue connecting"
                # my ssh command line
                p = pexpect.spawn(f"ssh {user}@{ip}", encoding="utf-8")
                i = p.expect([ssh_newkey, "password:", pexpect.EOF])
                if i == 0:
                    print("I say yes")
                    p.sendline("yes")
                    i = p.expect([ssh_newkey, "password:", pexpect.EOF])
                if i == 1:
                    print("I give password")
                    p.sendline(pw)
                    # p.expect("root")
                    # p.expect(pexpect.EOF)
                elif i == 2:
                    print("I either got key or connection timeout")
                    pass
                print(p.before)
                p.interact()
            except Exception as e:
                print("pxssh failed on login.")
                print(e)

    def get_ips(self, name):
        with open(self.file) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)

            ip = data["servers"][name]["ip"]
            user = data["servers"][name]["user"]
            pw = data["servers"][name]["pass"]
            try:
                ssh_newkey = "Are you sure you want to continue connecting"
                # my ssh command line
                p = pexpect.spawn(f"ssh {user}@{ip}", encoding="utf-8")
                i = p.expect([ssh_newkey, "password:", pexpect.EOF])
                if i == 0:
                    print("I say yes")
                    p.sendline("yes")
                    i = p.expect([ssh_newkey, "password:", pexpect.EOF])
                if i == 1:
                    print("I give password")
                    p.sendline(pw)
                    # p.expect("root")
                    # p.expect(pexpect.EOF)
                elif i == 2:
                    print("I either got key or connection timeout")
                    pass
                print(p.before)
                p.interact()
            except Exception as e:
                print("pxssh failed on login.")
                print(e)


if __name__ == "__main__":
    # Data("data").read_yaml()
    Data("data").login("racktables")
