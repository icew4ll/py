import datetime
import json
import time
from functools import wraps
from typing import Any, Callable


def inspect(fun):
    """"""
    print("Inspecting: ", fun)
    print(dir(fun))
    print(type(fun))


def jsoner(dict):
    print(json.dumps(dict, indent=1))


def timeit(func: Callable[..., Any]) -> Callable[..., Any]:
    """Times a function, usually used as decorator"""

    @wraps(func)
    def timed_func(*args: Any, **kwargs: Any) -> Any:
        """Returns the timed function"""
        start_time = time.time()
        result = func(*args, **kwargs)
        elapsed_time = datetime.timedelta(seconds=(time.time() - start_time))
        print(f"time spent on {func.__name__}: {elapsed_time}")
        return result

    return timed_func


# file, loc = ("lambda.py", "../tools/debug.py")
# tools = SourceFileLoader(file, loc).load_module()
# inspect = tools.inspect
