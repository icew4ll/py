import ipaddress
import os

import typer
from sqlalchemy import Column, Integer, MetaData, String, Table, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

app = typer.Typer()
engine = create_engine(f'sqlite:///{os.environ["db"]}')
Base = declarative_base()

# Sessionn = ORM's handle to the database
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()


class Server(Base):
    __tablename__ = "Servers"
    id = Column(Integer, primary_key=True)
    rack_id = Column(Integer)
    name = Column(String)
    ip = Column(Integer)
    eth = Column(String)

    def __repr__(self):
        return f"<Server(name='{self.name}', rack_id='{self.rack_id}', ip='{self.ip}, eth='{self.eth}')>"


@app.command()
def all():
    for i in session.query(Server):
        typer.secho(
            f"{i.rack_id},{i.name},{str(ipaddress.ip_address(int(i.ip)))},{i.eth}",
            fg="green",
        )


if __name__ == "__main__":
    # print(hello(1))
    app()
