"""test"""

from pynput import keyboard


def main():
    print("Press s or n to continue:")

    with keyboard.Events() as events:
        # Block for as much as possible
        event = events.get(1e6)
        if event.key == keyboard.KeyCode.from_char("s"):
            print("YES")


if __name__ == "__main__":
    main()
