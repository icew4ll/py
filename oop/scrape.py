from selenium import webdriver
from bs4 import BeautifulSoup
import requests
from termcolor import colored


class Boot:
    """docstring for Boot."""

    def __init__(self, url, tag, pre):
        self.url = url
        self.tag = tag
        self.pre = pre

    def get_links(self):
        print(colored(f'Scraping: {self.url}', 'yellow'))
        driver = webdriver.Firefox()
        driver.implicitly_wait(30)
        driver.get(self.url)
        soup = BeautifulSoup(driver.page_source, 'html.parser')

        for a_href in soup.find_all("a", href=True):
            if self.tag in a_href["href"]:
                yield a_href["href"]

    def get_first(self):
        url = f'{self.pre}{next(iter(self.get_links()))}'
        print(colored(f'Downloading: {url}', 'yellow'))
        return url

    def download(self):
        url = self.get_first()
        r = requests.get(url)
        file = url.split('/')[-1]

        with open(file, 'wb') as output_file:
            output_file.write(r.content)
        print(colored('Complete', 'green'))


if __name__ == "__main__":
    URL = 'https://github.com/dreemurrs-embedded/Jumpdrive/releases'
    TAG = 'pine64-pinephone.img.xz'
    PRE = 'https://github.com'
    # [print(i) for i in Boot(URL, TAG).get_first()]
    Boot(URL, TAG, PRE).download()
