class Person:
    # count variable for people
    number_of_people = 0
    # constants for object
    fall_rate = -9.8

    def __init__(self, name):
        self.name = name
        Person.add_person()

    # classmethods act on class itself and don't have access to any instance
    # cls act on class not on instance (self)
    @classmethod
    def number_of_people_cls(cls):
        return cls.number_of_people()

    @classmethod
    def add_person(cls):
        cls.number_of_people += 1


class Math:
    @staticmethod
    def add5(x):
        return x + 5

    @staticmethod
    def add10(x):
        return x + 10


print(Math.add10(5))
# p1 = Person("tim")
# print(Person.number_of_people)
# p2 = Person("jill")
# print(Person.number_of_people)
