from typing import List
from pydantic import BaseModel
import os
import yaml
from termcolor import colored


class Release(BaseModel):
    url: str
    tag: str
    pre: str


class Foo(BaseModel):
    count: int
    size: float = None


class Bar(BaseModel):
    apple = 'x'
    banana = 'y'


class Spam(BaseModel):
    foo: Foo
    bars: List[Bar]


def read_yaml(file):
    file = f"{os.getcwd()}/{file}"
    with open(file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        releases: List[Release] = [Release(**item) for item in data]
    return releases


class Scraper(BaseModel):
    rel: Release


def simple():
    m = Spam(foo={'count': 4}, bars=[{'apple': 'x1'}, {'apple': 'x2'}])
    print(m)
    print(m.dict())


def psimple():
    db = read_yaml("db.yml")
    # print(db)
    print(Scraper(rel=db[0]))


if __name__ == "__main__":
    print(colored('Complete', 'green'))
    psimple()
