"""
Dunder, magic method
"""


class Bongo:
    """return generator"""

    def __init__(self, n):
        self.n = n

    def __iter__(self):
        for i in range(self.n):
            yield i


for i in Bongo(20):
    print(i)
