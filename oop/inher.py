class Pet:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def show(self):
        print(f"I am {self.name}, {self.age} years old")

    def speak(self):
        print("I don't know what to say")


class Cat(Pet):
    def __init__(self, name, age, color):
        # super: reference super class method "__init__"
        # Get attributes "name", "age"
        super().__init__(name, age)
        self.color = color

    def speak(self):
        print("Meow")

    def show(self):
        print(f"I am {self.name}, {self.age} years old and {self.color}")


class Dog(Pet):
    def speak(self):
        print("Bark")


p = Pet("Shill", 19)
p.speak()
c = Cat("Bob", 34, "Brown")
c.show()
d = Dog("Bill", 25)
d.speak()
