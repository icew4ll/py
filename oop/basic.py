# <class 'int'> x is object of type int
x = 1
print(type(x))


class Dog:
    def __init__(self, name):
        # created attribute "name" of class dog
        self.name = name

    def get_name(self):
        return self.name


# <class '__main__.Dog'>
# main module contains class Dog
d = Dog("Shill")
d2 = Dog("Laddie")
print(type(d))
# two instances of dog objects
# get_name refers to "self" instance of object
print(f"Doggo names: {d.get_name()} {d2.get_name()}")


class Human:
    # properties 
    def __init__(self, n, o):
        self.name = n
        self.occupation = o

    # methods 
    def do_work(self):
        if self.occupation == "tennis player":
            print(self.name, "plays tennis")
        elif self.occupation == "actor":
            print(self.name, "shoots film")

    def speaks(self):
        print(self.name, "how are you?")

tom = Human("Tom Cruise", "actor")
tom.do_work()
tom.speaks()

maria = Human("Maria Sharapova", "tennis player")
maria.do_work()
maria.speaks()
