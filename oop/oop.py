class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


class Student(Person):
    def __init__(self, name, age, email):
        super().__init__(name, age)
        self.email = email


if __name__ == "__main__":
    p1 = Student("test", 25, "test@test.com")
    print(vars(p1))
