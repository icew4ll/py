import json

import requests


def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)


if __name__ == "__main__":
    response = requests.get("https://api.coingecko.com/api/v3/ping")
    print(response.status_code)
    jprint(response.json())
