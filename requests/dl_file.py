import os
import urllib.request
from importlib.machinery import SourceFileLoader
from io import BytesIO
from zipfile import ZipFile

from bs4 import BeautifulSoup
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

from requests import get

file, loc = ("debug.py", "../tool/debug.py")
tools = SourceFileLoader(file, loc).load_module()
inspect = tools.inspect
timeit = tools.timeit


def zip_dl(url):
    """zip dl"""
    loc = f'{os.environ["HOME"]}/.local/share/fonts'
    url = urllib.request.urlopen(url)
    with ZipFile(BytesIO(url.read())) as zip_object:
        files = [file for file in zip_object.namelist()]
        completer = WordCompleter(files)
        print(files, end="\n")
        while True:
            font = prompt("Font: ", completer=completer, complete_while_typing=False,)
            print(f"Downloading {font} to: \n{loc}")
            zip_object.extract(font, path=loc)


def get_html(url):
    print(f"Getting HTML from: \n{url}")
    res = get(url)
    return res.text


def get_links(html):
    print("Getting links...")
    soup = BeautifulSoup(html, "html.parser")
    href = soup.find_all("a", href=True)
    return (a.get("href") for a in href)


def get_dls(links):
    print("Getting dls...")
    tag = None
    for link in links:
        if "download" in link:
            tag = link.split("/")[5]
            break
    fonts = {
        link.split("/")[6].replace(".zip", "") for link in links if "download" in link
    }
    return fonts, tag


def completer(data):
    fonts, tag = data
    font_completer = WordCompleter(fonts)
    print(fonts, end="\n")
    while True:
        text = prompt("Fonts: ", completer=font_completer, complete_while_typing=False,)
        repo = "ryanoasis/nerd-fonts"
        url = f"https://github.com/{repo}/releases/download/{tag}/{text}.zip"
        zip_dl(url)


def write_file(html):
    with open("links", "wt") as out:
        out.write(str(html))


def read_file(url):
    with open("links", "rt") as file:
        data = file.read()
    return data


if __name__ == "__main__":
    print("\nRESET...")
    url = "https://github.com/ryanoasis/nerd-fonts/releases"
    completer(get_dls(get_links(get_html(url))))
