import requests
from bs4 import BeautifulSoup
from PyPDF2 import PdfFileReader

# from io import StringIO
from io import BytesIO

# from StringIO import StringIO
# import os
# import urllib2
# from io import BytesIO

url = (
    "https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports"
)

# get link
r = requests.get(url)
soup = BeautifulSoup(r.content, "html.parser")
div_id = soup.find("div", {"id": "PageContent_C006_Col01"})
a = div_id.find_all("a")
print(a[0])
test = ""
for b in a[0].find_all("a", href=True):
    test = "https://www.who.int" + b["href"]
print(test)


# def text_extractor(path):
# with open(path, "rb") as f:
# pdf = PdfFileReader(f)

# # get the first page
# page = pdf.getPage(1)
# print(page)
# print("Page type: {}".format(str(type(page))))

# text = page.extractText()
# print(text)


# request 2
r2 = requests.get(test)
# print(r2.content)
pdf_file = BytesIO(r2.content)
# text_extractor(pdf_file)
pdf = PdfFileReader(pdf_file)
page = pdf.getPage(4)
print(page)
text = page.extractText()
print(text)
# print(pdf)

# request report
# https://www.who.int/docs/default-source/coronaviruse/situation-reports/20200213-sitrep-24-covid-19.pdf?sfvrsn=9a7406a4_4
# print("https://www.who.int/", )
