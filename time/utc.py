from datetime import datetime, timezone, timedelta
from time import strftime, localtime
from dateutil import tz

utc = datetime.utcnow()
print(f"utc: {utc}")

now = datetime.now(timezone.utc)
print(f"local: {now}")

date_local = strftime("%A, %d %b %Y", localtime())
print(date_local)

# METHOD 1: Hardcode zones:
from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/New_York')

# METHOD 2: Auto-detect zones:
from_zone = tz.tzutc()
to_zone = tz.tzlocal()

utc = datetime.utcnow()
print(f"now: {utc}")
# utc = datetime.strptime('2011-01-21 02:37:21', '%Y-%m-%d %H:%M:%S')

# Tell the datetime object that it's in UTC time zone since
# datetime objects are 'naive' by default
utc = utc.replace(tzinfo=from_zone)

# Convert time zone
fmt = '%Y-%m-%d %H:%M:%S %Z%z'
central = utc.astimezone(to_zone).strftime(fmt)
print(f"central: {central}")
