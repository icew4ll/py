from datetime import datetime, timedelta
from pytz import timezone
import pytz

utc = pytz.utc
print(utc.zone)
eastern = timezone("US/Eastern")
print(eastern.zone)
amsterdam = timezone("Europe/Amsterdam")
fmt = "%Y-%m-%d %H:%M:%S %Z%z"

# localize time
loc_dt = eastern.localize(datetime(2002, 10, 27, 6, 0, 0))
print(f"localized time: { loc_dt.strftime(fmt) }")

ams_dt = loc_dt.astimezone(amsterdam)
print(f"localized time2: { ams_dt.strftime(fmt) }")

# tzinfo argument of standard datetime works only with utc
tzinfo = datetime(2002, 10, 27, 12, 0, 0, tzinfo=pytz.utc).strftime(fmt)
print(f"utc {tzinfo}")

# preferred method: work in utc converting to localtime only when generating output
# local 2002-10-27 01:00:00 EST-0500
# EST is 5 hours behind UTC and observed in winter
# EDT is 4 hours behind UTC and observed in summer
# utc dt
utc_dt = datetime(2002, 10, 27, 6, 0, 0, tzinfo=pytz.utc)
# localize utc datetime
loc_dt = utc_dt.astimezone(eastern)
# format for output
local = loc_dt.strftime(fmt)
print(f"local {local}")

# Convert UTC --> US/Eastern
# UTC: Monday, September 28, 2020 at 18:51:00
# EDT: 2020-09-28 14:51:00 EDT-0400
# utc dt year, month, day
utc_dt = datetime(2020, 9, 28, 18, 51, 0, tzinfo=pytz.utc)
# localize utc datetime
loc_dt = utc_dt.astimezone(eastern)
# format for output
local = loc_dt.strftime(fmt)
print(f"local {local}")

# Convert local time --> UTC
# local = pytz.timezone("America/Los_Angeles")
tz = "Australia/Brisbane"
time = "2020-9-30 5:28:00"
local = pytz.timezone(tz)
# parse string into naive datetime object
naive = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
# find local timezone
local_dt = local.localize(naive, is_dst=None)
# convert to UTC
utc_dt = local_dt.astimezone(pytz.utc)
print(f"{tz} {time} --> UTC {utc_dt}")

# list all timezones
# pytz.all_timezones
print(pytz.all_timezones)

# Convert UTC --> US/Eastern
# UTC: Monday, September 28, 2020 at 18:51:00
# EDT: 2020-09-28 14:51:00 EDT-0400
# utc dt year, month, day
tz = "Australia/Brisbane"
time = "2020-9-28 18:50:00"
local = timezone(tz)
utc_dt = datetime(2020, 9, 29, 19, 28, 0, tzinfo=pytz.utc)
# localize utc datetime
loc_dt = utc_dt.astimezone(local)
# format for output
out = loc_dt.strftime(fmt)
print(f"UTC -> {tz} {out}")


# Australia/Brisbane 2020-9-30 5:28:00 --> UTC 2020-09-29 19:28:00+00:00
# UTC -> Australia/Brisbane 2020-09-30 05:28:00 AEST+1000

# 5:30 PM (17:30) Queensland Time =
#  7:30 AM (7:30) UTC
# 6:00 PM (18:00) Queensland Time =
#  8:00 AM (8:00) UTC
# 6:30 PM (18:30) Queensland Time =
#  8:30 AM (8:30) UTC
