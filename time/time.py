from datetime import datetime, timedelta

import pytz
import typer
from pytz import timezone

app = typer.Typer()


@app.command()
def hello(name: str):
    eastern = timezone("US/Eastern")
    bogota = timezone("America/Bogota")
    fmt = "%Y-%m-%d %H:%M:%S %Z%z"

    utc_dt = datetime(2002, 10, 27, 6, 0, 0, tzinfo=pytz.utc)
    # localize utc datetime
    loc_dt = utc_dt.astimezone(eastern)
    # format for output
    local = loc_dt.strftime(fmt)
    # EST is 5 hours behind UTC and observed in winter
    # EDT is 4 hours behind UTC and observed in summer
    print(f"local {local}")

    typer.secho(f"hi {name}", fg="red")


@app.command()
def local2utc(time: str, zone: str):
    """ rep local2utc '2020-4-21 5:37:00' 'America/Panama' """
    # tz = "America/Bogota"
    # tz = "America/Bogota"
    # time = "2020-4-20 8:28:00"
    # time = "2020-4-20 8:28:00"
    local = pytz.timezone(zone)
    # parse string into naive datetime object
    naive = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
    # find local timezone
    local_dt = local.localize(naive, is_dst=None)
    # convert to UTC
    utc_dt = local_dt.astimezone(pytz.utc)
    print(f"{zone} {time} --> UTC {utc_dt}")


def utc2local(time: str, zone: str):
    fmt = "%Y-%m-%d %H:%M:%S %Z%z"
    # tz = "Africa/Juba"
    # time = "2020-4-20 15:28:00"
    local = timezone(zone)
    utc_dt = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
    # localize utc datetime
    loc_dt = utc_dt.astimezone(local)
    # format for output
    out = loc_dt.strftime(fmt)
    print(f"UTC -> {zone} {out}")


@app.command()
def timezones():
    """ timezones """
    for i in pytz.all_timezones:
        print(i)


if __name__ == "__main__":
    app()
