import os


def ternary():
    """ternary operator"""
    condition = False
    x = 1 if condition else 0
    print(x)


def underscores():
    """underscores make numbers readable without effecting math"""
    num1 = 10_000_000_000
    num2 = 100_000_000
    total = num1 + num2
    # comma separate numbers
    print(f"{total:,}")


def createFile():
    """create file in pwd"""
    pwd = os.getcwd()
    file = f"{pwd}/test.txt"
    print(f"pwd: { pwd }")
    # context managers: with open
    # instead of opening and closing files manually
    # use a context manager to manage the open resources
    # file will be properly closed after file operations completed
    # even in the event of an exception
    # use cases: file, threads, locks

    try:
        # write and read file
        with open(file, "w+") as f:
            print(f"{file} found")
            for i in range(10):
                f.write("This is line %d\r\n" % (i + 1))

        with open(file, "r") as f:
            file_contents = f.read()
            print(file_contents)

    except FileNotFoundError:
        # + will create file if it doesn't exist
        # r 	Open file for reading
        # w 	Open file for writing (will truncate file)
        # b 	binary more
        # r+ 	open file for reading and writing
        # a+ 	open file for reading and writing (appends to end)
        # w+ 	open file for reading and writing (truncates files)
        # x to create file
        with open(file, "x"):
            print(f"{file} created")


def enumerateUse():
    # enumerate use case:
    # counter while looping
    # returns index and value from collection one is looping
    name = ["test", "t2", "t3"]
    for index, name in enumerate(name, start=1):
        print(index, name)


def zipUse():
    l1 = [
        "t1",
        "t1",
        "t1",
    ]
    l2 = [
        "e1",
        "e1",
        "e1",
    ]
    l3 = [
        "s1",
        "s1",
        "s1",
    ]
    for value in zip(l1, l2, l3):
        print(f"{value}")
    # unpacking tuples into vars
    for t, e, s in zip(l1, l2, l3):
        print(f"{t} zip {e} zip {s}")


def unpacking():
    a, b = (1, 2)
    # unpack only first element
    c, _ = (1, 2)
    # last variable will contain remaining elements
    d, e, *f = (1, 2, 3, 4, 5)
    # remaining elements will be ignored
    g, h, *_ = (1, 2, 3, 4, 5)
    # extra elements stored in star, last var has last element
    j, k, *l, m = (1, 2, 3, 4, 5)
    print(a, b, c, d, e, f, g, h, j, k, l, m)


def classAttr():
    """class attribute access"""

    class Person:
        pass

    # dynamically set class attributes
    person = Person()
    person.first = "Shill"
    person.last = "Mason"
    print(f"{person.first} {person.last}")
    first_key = "first"
    first_val = "Shill"

    # setattr: use case, set attributes with variables
    setattr(person, first_key, first_val)
    print(person.first)

    # getattr: use case, get attributes with variables
    first = getattr(person, first_key)
    print(first)

    # set object attributes with dictionary loop
    person_info = {"first": "Shill", "last": "Mason"}
    for key, value in person_info.items():
        setattr(person, key, value)
    print(f"{person.first} {person.last}")

    for key in person_info.keys():
        print(getattr(person, key))


if __name__ == "__main__":
    # underscores()
    # ternary()
    # enumerateUse()
    createFile()
    # zipUse()
    # unpacking()
    # classAttr()
