import glob
import os
import sys

f1 = f"{ os.getcwd() }/test.txt"


def write_file():
    """docstring for write_file"""
    with open(f1, "wt") as out:
        print("Hello World", file=out)


def read_file():
    with open(f1, "rt") as file:
        for line in file:
            print(line)


def arg_parse():
    file, arg1 = sys.argv
    print(f"file: {file}, arg1: {arg1}")


def unpack():
    l1 = list("test")
    c1, c2, c3, c4 = l1
    print(f"{c1}{c2}{c3}{c4}")


def list_files():
    pwd = os.getcwd()
    os.chdir(pwd)
    for file in glob.glob("*"):
        print(f"{pwd}/{file}")


if __name__ == "__main__":
    print("RESET")
    list_files()
    # unpack()
    # arg_parse()
    # write_file()
    # read_file()
