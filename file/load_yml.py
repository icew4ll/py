import ipaddress
import json
import os
import re
import sys
import traceback
from types import SimpleNamespace

import paramiko
import yaml
from paramiko_expect import SSHClientInteraction
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter
from box import Box

movie_box = Box({ "Robin Hood: Men in Tights": { "imdb stars": 6.7, "length": 104 } })

movie_box.Robin_Hood_Men_in_Tights.imdb_stars

file = os.environ["write"]
with open(file, "r") as stream:
    try:
        data = Box(yaml.safe_load(stream))
        # sns = SimpleNamespace(**data)
    except yaml.YAMLError as exc:
        print(exc)


class Struct2:
    def __init__(self, **entries):
        self.__dict__.update(entries)

    # def __repr__(self):
    #     return f"{self.items()}"

    # def __str__(self):
    #     return f"{self}"


class Struct:
    def __init__(self, **entries):
        dict(self).update(data)

    def pdata(self):
        print(self)

    def data(self):
        return list(self.data)


def main():
    print("=" * 30)
    print(data)
    # s = Struct2(**data)
    # print(s)
    # Servers().pdata()
    # print(Servers().data())
    # completer = WordCompleter(Servers().data())
    # while True:
    #     text = prompt(
    #         "Give some animals: ",
    #         completer=completer,
    #         complete_while_typing=True,
    #     )
    #     print("You said: %s" % text)


if __name__ == "__main__":
    main()
