# -*- coding: utf-8 -*-
import io
import os
import yaml

file = os.environ["write"]
with open(file, 'r') as stream:
    try:
        print(yaml.safe_load(stream))
    except yaml.YAMLError as exc:
        print(exc)

# with open('newtree.yaml', "w") as f:
#     yaml.dump(dataMap, f)

# Define data
data = {
    'a list': [1, 42, 3.141, 1337, 'help', u'€'],
    'a string': 'bla',
    'another dict': {
        'foo': 'bar',
        'key': 'value',
        'the answer': 42
    }
}

# Write YAML file
with io.open('data.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)

# Read YAML file
with open("data.yaml", 'r') as stream:
    data_loaded = yaml.safe_load(stream)

# Read YAML file 2
with open("data.yml") as f:
    data = yaml.load(f, Loader=yaml.FullLoader)

print(data == data_loaded)
