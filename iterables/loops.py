def for_loop():
    for i in range(0, 10):
        print(i)


def while_loop():
    base, index = (10, 0)
    while index < base:
        print(index)
        index += 1


if __name__ == "__main__":
    # for_loop()
    while_loop()
