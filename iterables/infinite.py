import itertools


def infinite_iter_count():
    for i in itertools.count(5, 5):
        if i == 35:
            break
        else:
            print(i, end=" ")


def infinite_iter_cycle():
    for index, i in enumerate(itertools.cycle("AB")):
        if index > 7:
            break
        else:
            print(i, end=" ")


def infinite_iter_repeat():
    print(list(itertools.repeat(25, 4)))


if __name__ == "__main__":
    infinite_iter_count()
    # 5 10 15 20 25 30
    infinite_iter_cycle()
    # A B A B A B A B
    infinite_iter_repeat()
    # [25, 25, 25, 25]
