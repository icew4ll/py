def iterable(n):
    """determine iterablility with __iter__ method using dir()"""
    print(type(n), dir(n))


class MyRange:
    """creating an iterable"""
    def __init__(self, start, end):
        self.value = start
        self.end = end

    def __iter__(self):
        return self

    def __next__(self):
        if self.value >= self.end:
            raise StopIteration
        current = self.value
        self.value += 1
        return current


if __name__ == "__main__":
    print("\nRESET...")
    # nums = range(1, 3)
    nums = [1, 2, 3]
    # [print(i) for i in nums]
    # print(list(map(lambda i: print(i), nums)))
    # print(map(lambda x: x, nums))
    for i in nums:
        print(i)
