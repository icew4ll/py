import itertools
import json


def inspect(fun):
    """"""
    print("Inspecting: ", fun)
    print(dir(fun))
    print(type(fun))


def jsoner(dict):
    print(json.dumps(dict, indent=1))


def lambda_fun():
    # named function
    def add5(num):
        return num + 5

    num = 7
    print(add5(num))

    # lambda equivalent
    print((lambda x: x + 5)(num))


def sort_list_tuples():
    """sorting a list of tuples"""
    i1 = zip("test", "abcd")
    inspect(i1)
    # for loop on zip object (iterable) causes values to be exhausted
    # ie converting to list afterwards exhaustion creates an empty list
    # for i in i1:
    #     print(i)

    # convert zip object (iterable) to list, to allow list methods ie sort
    l1 = list(i1)
    inspect(l1)

    # sort by first index
    l1.sort(key=lambda i: i[0])
    print(l1)
    # sort by second index, using variable reassignment
    l2 = sorted(l1, key=lambda i: i[1])
    print(l2)


def sort_list_dicts():
    keys = ("make", "model", "year")
    list_of_tuples = [
        ("acura", "rsx", 2001),
        ("honda", "accord", 1999),
        ("infinity", "g20", 2000),
    ]
    list_of_dict = [dict(zip(keys, values)) for values in list_of_tuples]
    inspect(list_of_dict)
    s_year = sorted(list_of_dict, key=lambda i: i["year"])
    s_model = sorted(list_of_dict, key=lambda i: i["model"])
    s_make = sorted(list_of_dict, key=lambda i: i["make"])
    jsoner(s_year)
    jsoner(s_model)
    jsoner(s_make)


def filter_list():
    """filter list"""
    l1 = list(range(1, 7))
    inspect(l1)
    l2 = list(filter(lambda i: i % 2 == 0, l1))
    inspect(l2)


def map_list():
    """map list applies lambda to every element in list"""
    l1 = list(range(1, 7))
    l2 = list(map(lambda i: i + 2, l1))
    inspect(l2)


def one_liner():
    print({x: x ** 2 for x in (-y for y in range(5))})


if __name__ == "__main__":
    # filter_list()
    map_list()
