import datetime
import time
from functools import wraps
from typing import Any, Callable

# from memory_profiler import profile


def timeit(func: Callable[..., Any]) -> Callable[..., Any]:
    """Times a function, usually used as decorator"""

    @wraps(func)
    def timed_func(*args: Any, **kwargs: Any) -> Any:
        """Returns the timed function"""
        start_time = time.time()
        result = func(*args, **kwargs)
        elapsed_time = datetime.timedelta(seconds=(time.time() - start_time))
        print(f"time spent on {func.__name__}: {elapsed_time}")
        return result

    return timed_func


class Generator:
    def __init__(self, gen_func):
        self.gen_func = gen_func

    def driver(self):
        """docstring for generator_driver"""
        # list comprehension as driver increases memory usage!
        lambda i: i, self.gen_func


@timeit
def square_list(nums):
    result = []
    for i in nums:
        result.append(i * i)
    return result


@timeit
def square_list_exp(nums):
    return [x * x for x in nums]


@timeit
def square_gen(nums):
    for i in nums:
        yield i * i


@timeit
def square_gen_exp(nums):
    yield (x * x for x in nums)


if __name__ == "__main__":
    print("\nRESET...")
    nums = range(1, 10000)
    funs = (
        square_gen_exp(nums),
        square_gen(nums),
        square_list(nums),
        square_list_exp(nums),
    )
    lambda fun: Generator(fun).driver(), funs
