from typing import Iterator, List


def iterable():
    t = "test"
    for i in t:
        yield i


def print_iterable(iter: Iterator[str]):
    for i in iter:
        print(i)


def last_v1(iter: Iterator[str]):
    *_, last = iter
    print(last)


def last_v2(iter: Iterator[str]):
    item = None
    for item in iter:
        pass
    print(item)


if __name__ == "__main__":
    print_iterable(iterable())
    last_v1(iterable())
