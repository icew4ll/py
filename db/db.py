from sqlalchemy import (Column, Integer, MetaData, String, create_engine,
                        inspect)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import create_database, database_exists

# create database, echo sets up logging
engine = create_engine("sqlite:///test.db", echo=True)
# dialect[+driver]://user:password@host/dbname
if not database_exists(engine.url):
    create_database(engine.url)

print(database_exists(engine.url))

# inspect all tables/column names
inspector = inspect(engine)

for table_name in inspector.get_table_names():
    for column in inspector.get_columns(table_name):
        print("Column: %s" % column["name"])

# inspect all tables/column names via reflect
m = MetaData()
m.reflect(engine)
for table in m.tables.values():
    print(table.name)
    for column in table.c:
        print(column.name)

Base = declarative_base()
