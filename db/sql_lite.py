from sqlalchemy import (Column, Integer, MetaData, String, Table,
                        create_engine, inspect)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import create_database, database_exists

# create database, echo sets up logging
engine = create_engine("sqlite:///test.db", echo=True)


Base = declarative_base()


class Person(Base):
    __tablename__ = "Person"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)


# create session
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

# insert and update
bob = Person(name="Bob", age=21)
session.add(bob)
session.commit()

# query all employees named Alice
# res = session.query(Employee).filter_by(name='Alice').all()
