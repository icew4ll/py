from sqlalchemy import (Column, Integer, MetaData, String, Table,
                        create_engine, inspect)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import create_database, database_exists

# create database, echo sets up logging
engine = create_engine("sqlite:///test.db", echo=True)


# create table with name "students"
meta = MetaData()

human = Table(
    "Human",
    meta,
    Column("id", Integer, primary_key=True),
    Column("name", String),
    Column("age", Integer),
)

# create/drop table
human.create(engine)
# human.drop(engine)


Base = declarative_base()


class Human(Base):
    __tablename__ = "Human"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)


# create session
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

# insert and update
bob = Human(name="Bob", age=21)
session.add(bob)
session.commit()
