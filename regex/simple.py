import re


def find_all():
    p1 = re.compile(r"\d+")
    s1 = "12 drummers drumming, 11 pipers piping, 10 lords a-leaping"
    print(p1.findall(s1))


def find_match():
    t1 = [
        "Manjaro-ARM-phosh-pinephone-beta13-pkgs.txt",
        "Manjaro-ARM-phosh-pinephone-beta13.img.xz",
        "Manjaro-ARM-phosh-pinephone-beta13.img.xz.sha1",
    ]
    reg = r"Manjaro-ARM-phosh-pinephone.*xz$"
    # r prefix: raw string
    p2 = re.compile(reg)
    for i in t1:
        if p2.match(i):
            print(i)


if __name__ == "__main__":
    find_match()
