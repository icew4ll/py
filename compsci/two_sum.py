def two_sum(arr, target):
    """docstring for two_sum"""
    left = 0
    right = len(arr) - 1
    while left < right:
        curr = arr[left] + arr[right]
        if curr < target:
            left += 1
        elif curr > target:
            right -= 1
        else:
            return [left, right]
    return [-1, -1]


arr1 = [2, 7, 11, 15]
tar1 = 9
arr2 = [2, 3, 4]
tar2 = 6
arr3 = [1, 3, 5, 7]
tar3 = 100

print(two_sum(arr1, tar1))
print(two_sum(arr2, tar2))
print(two_sum(arr3, tar3))
