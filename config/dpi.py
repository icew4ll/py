import os
import inspect
from termcolor import colored
# import pprint
import json

home = os.environ["HOME"]
dpi = 100
display = "95 100"
screen = "HDMI-0"

d1 = {
    '/usr/lib/X11/xdm/Xsetup_0': inspect.cleandoc(f'''
        xrandr --dpi {dpi}
    '''),
    '/usr/share/X11/xorg.conf.d/90-monitor.conf': inspect.cleandoc(f'''
        Section "Monitor"
            Identifier "{screen}"
            DisplaySize  {display}
        EndSection
    '''),
    f'{home}/.Xresources': inspect.cleandoc(f'''
        Xft.dpi: {dpi}
        Xft.antialias: true
        Xft.rgba: rgb
        Xft.hinting: true
        Xft.hintstyle: hintslight
    '''),

}
# pprint.pprint(d1)
# colored('Hello, World!', 'red', attrs=['reverse', 'blink'])
obj = json.dumps(d1, sort_keys=False, indent=4)
print(colored(obj, 'red'))

for file, content in d1.items():
    print(colored(file, 'green'), content)
    with open(file, "w") as item:
        item.write(content)
