def fib_gen(lst):
    a, b = 0, 1
    while a <= max(lst):
        yield a
        a, b = b, a + b


if __name__ == "__main__":
    print("\nRESET...")
    # lst = [1, 4, 3, 9, 10, 13, 7]
    # lst = [0, 2, 8, 5, 2, 1, 4, 13, 23, 1, 4]
    lst = [i for i in range(0, 100)]
    fib_list = [i for i in fib_gen(lst)]
    # fib_list = (i for i in fib_gen(lst))
    print(lst)
    for i in lst:
        if i in fib_list:
            print(i, end=" ")
