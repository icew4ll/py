def fib(n):
    """get nth fibonacci number"""
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


cache = {}


def fibm(n):
    """get nth fibonacci number, using memoization"""
    # if value is cached, return it
    if n in cache:
        return cache[n]
    # compute nth term
    if n == 1:
        return 0
    elif n == 2:
        return 1
    elif n > 2:
        val = fibm(n - 1) + fibm(n - 2)
    cache[n] = val
    return val


def fibd(n, stash={}):
    """get nth fibonacci number, using dynamic programming"""
    if n in stash:
        return stash[n]
    if n == 1:
        return 0
    elif n == 2:
        return 1
    elif n > 2:
        val = fibd(n - 1, stash) + fibd(n - 2, stash)
    stash[n] = val
    return val


if __name__ == "__main__":
    # print(fib(50))
    # driver
    # print(f"{fib(2)}")
    for n in range(1, 10):
    #     # print(n, ":", fib(n))
        print(f"n: {n}, val: {fibm(n)} {cache}")
