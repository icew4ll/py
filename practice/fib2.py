import datetime
import time
from functools import wraps
from typing import Any, Callable

# fibonacci number
# return the nth number in a fibonacci sequence
# 0, 1, 1, 2, 3, 5, 8


def timeit(func: Callable[..., Any]) -> Callable[..., Any]:
    """Times a function, usually used as decorator"""

    @wraps(func)
    def timed_func(*args: Any, **kwargs: Any) -> Any:
        """Returns the timed function"""
        start_time = time.time()
        result = func(*args, **kwargs)
        elapsed_time = datetime.timedelta(seconds=(time.time() - start_time))
        print(f"time spent on {func.__name__}: {elapsed_time}")
        return result

    return timed_func


def fibonacci(n):
    """fibonacci """
    result = []
    for index, el in enumerate(range(0, n)):
        if index == 0:
            result.append(0)
        elif index == 1:
            result.append(1)
        else:
            result.append(result[index - 2] + result[index - 1])
    print(result[-1])


@timeit
def fib2(n):
    """fibonacci with variable unpacking"""
    a, b = 0, 1
    for el in range(0, n):
        # print(a)
        a, b = b, a + b


@timeit
def fib_generator(n):
    a, b = 0, 1
    for i in range(0, n):
        yield f"{i + 1}, {a}"
        a, b = b, a + b


@timeit
# ???
def fibo(n):
    if n <= 1:
        yield n
    else:
        yield fibo(n - 1) + fibo(n - 2)


@timeit
def test(n):
    time.sleep(n)
    print("done")


if __name__ == "__main__":
    print("\nRESET test")
    # test(2)
    iterations = 100_000_000
    # [print(i) for i in fib_generator(iterations)]
    # fib2(iterations)
    fibo(100)
    # [print(i) for i in fibo(iterations)]
