# for range n numbers
# if number is divisible by 3 print fizz
# if number is divisible by 5 print buzz
# if number is divisible by 3 and 5 print fizzbuzz

def fizzbuzz(n):
    """fizzbuzz"""
    for el in range(1, n+1):
        if el % 3 == 0 and el % 5 == 0:
            print("FizzBuzz")
        elif el % 3 == 0:
            print("Fizz")
        elif el % 5 == 0:
            print("Buzz")
        else:
            print(el)


if __name__ == "__main__":
    print("\nRESET")
    fizzbuzz(50)
