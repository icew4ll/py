from itertools import repeat


def delimiter():
    """delimiter"""
    l1 = ",".join(list("rekt"))
    print(l1)
    # l2 = []
    # for i in range(0, len(l1)):
    #     l2.append(",")

    # result = [y for x in list(zip(l1, l2)) for y in x]
    # result.pop()
    # print(result)
    # print([i for i in result])


# for element, comma in zip(l1, l2):
#     print(f"{element} {comma}")


def flatten():
    """docstring for flatten"""
    l1 = list("rekt")
    list_of_tuples = list(zip(l1, l1))
    print([y for x in list_of_tuples for y in x])


def repeat_vals():
    print(list(repeat(0, 6)))


if __name__ == "__main__":
    # delimiter()
    # flatten()
    repeat_vals()
