# sub problems:
# Dictionary order will be used to:
# - prioritize negative notation
# - prioritize largest numerical bases
# 1) loop through dictionary, return first occurence and remove it from string
# 2) loop on string until all letters are exhausted

def roman(str):
    """"""
    # order matters, in order to prioritize negative indexes
    map = {
        900: "CM",
        1000: "M",
        400: "CD",
        500: "D",
        90: "XC",
        100: "C",
        40: "XL",
        50: "L",
        9: "IX",
        10: "X",
        4: "IV",
        5: "V",
        1: "I",
    }

    def finder(str):
        """"""
        # loop through map until str is exhausted
        while len(str) != 0:
            # find numerical mapping for values
            for k, v in map.items():
                if v in str:
                    yield k
                    # remove first occurence of find
                    str = str.replace(map[k], "", 1)

    print(sum(finder(str)))


if __name__ == "__main__":
    print("\nRESET")
    # s1 = "IX"
    # s1 = "MCMIV"
    # s1 = "XL"
    # s1 = "XIII"
    s1 = "VIII"
    roman(s1)
