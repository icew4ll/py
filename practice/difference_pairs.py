from itertools import combinations


def pairs(n, k):
    for a, b in combinations(n, 2):
        if abs(a - b) == k:
            yield (a, b)
        else:
            pass


if __name__ == "__main__":
    # l1 = [8, 12, 16, 4, 0, 20]
    # k = 4
    l1 = [1, 5, 3, 4, 2]
    k = 3
    print(sum(1 for x in pairs(l1, k)))
