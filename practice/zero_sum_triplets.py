import itertools


def zero_sum_triplets(lst):
    combos = list(itertools.combinations(lst, 3))
    for index, i in enumerate(combos):
        if sum(list(i)) == 0:
            print(combos[index], end="")


if __name__ == "__main__":
    print("\nRESET")
    l1 = [0, -1, 2, -3, 1]
    # combos = list(itertools.combinations(l1, 3))
    # permu = list(itertools.permutations(l1, 3))
    # print("\n Combos:", combos, "\n Permutations:", permu)
    print("Triplets with zero sum: ", end="")
    zero_sum_triplets(l1)
