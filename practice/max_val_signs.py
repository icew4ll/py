from rich.traceback import install

install()


def max_val(str):

    # Store first character as integer
    # in result
    res = int(str[0])

    # Start traversing the string starting from second element
    for i in range(1, len(str)):

        # Check if any of the two numbers
        # is 0 or 1, If yes then add current
        # element
        if str[i] == "0" or str[i] == "1" or res < 2:
            res += int(str[i])
        else:
            res *= int(str[i])

    print(res)


if __name__ == "__main__":
    print("\nRESET")
    s1 = "891"
    # s1 = "01231"
    # s1 = "1231"
    max_val(s1)
