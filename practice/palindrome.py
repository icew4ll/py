def palindrome(n):
    if len(n) % 2 == 0:
        print("even")
        tag = len(n) / 2
    else:
        print("odd")
        tag = (len(n) - 1) / 2

    first = n[: int(tag)]

    last = n[int(tag) + 1 :]

    if last == first[::-1]:
        print("is palindrome")
    else:
        print("is not palindrome")

    # print(first, first[::-1], last)


def palindrome_v2(n):
    reversed_string = n[::-1]
    if n == reversed_string:
        print("is palindrome")
    else:
        print("is not palindrome")


def palindrome_v3(n):
    print(isPalindrome := n == n[::-1])


if __name__ == "__main__":
    n = "wooxxow"
    # palindrome_v2(n)
    palindrome_v2(n)
