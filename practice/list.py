class Expense:
    def __init__(self, elist):
        self.elist = elist

    def plist(self):
        return self.elist

    def diffFebJan(self):
        return self.elist[1] - self.elist[0]

    def firstQuarterExp(self):
        return sum(self.elist[0:3])

    def find2000(self):
        tmp = 2000
        if tmp in set(self.elist):
            return f"{tmp} found"
        else:
            return f"{tmp} not found"

    def addItem(self, n):
        self.elist.append(n)
        return self.elist

    def refundApril(self, amount):
        return self.elist[3] - amount


list = [2200, 2350, 2600, 2130, 2190]
e1 = Expense(list)
print(f"list: {e1.plist()}")
print(f"Diff Feb-Jan: {e1.diffFebJan()}")
print(f"firstQuarterExp: {e1.firstQuarterExp()}")
print(f"find2000: {e1.find2000()}")
print(f"append: {e1.addItem(1980)}")
print(f"refund: {e1.refundApril(200)}")


class Heroes:
    def __init__(self, heroes):
        self.heroes = heroes

    def length(self):
        print(f"length of {len(self.heroes)}")

    def indexOf(self, val):
        print(f"index of {self.heroes.index(val)}")

    def insertAfter(self, tag, item):
        self.heroes.insert(self.heroes.index(tag) + 1, item)
        print(f"new list: {self.heroes}")

    def remove(self, unwanted):
        print(f"unwanted: {[i for i in self.heroes if i not in unwanted]}")

    def sort(self):
        self.heroes.sort()
        print(f"sorted: {self.heroes}")


h = ["spider man", "thor", "hulk", "iron man", "captain america"]
h1 = Heroes(h)
h1.length()
h1.indexOf("hulk")
h1.insertAfter("hulk", "black panther")
h1.remove({"hulk", "thor"})
h1.sort()


def odder(max):
    result = []
    for i in range(1, max):
        if i % 2 == 1:
            result.append(i)
    return result


print(f"odd numbers {odder(100)}")
