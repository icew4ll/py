def intersection_lists():
    l1 = [i for i in range(0, 10)]
    l2 = [i for i in range(2, 12)]
    print(l1, l2)

    # brute force
    result = []
    for i in l1:
        if i in l2:
            result.append(i)
    print(result)

    # set union
    print(set(l1) & set(l2))

    # lambda/filter
    print(list(filter(lambda i: i in l2, l1)))


if __name__ == "__main__":
    intersection_lists()
