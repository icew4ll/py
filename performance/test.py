from importlib.machinery import SourceFileLoader
from itertools import repeat

file, loc = ("debug.py", "../tool/debug.py")
tools = SourceFileLoader(file, loc).load_module()
inspect = tools.inspect
timeit = tools.timeit


# l1 = list(repeat("test", 10000))

# for link in l1:
#     print(link, end="\n")

@timeit
def test():
    l1 = list(repeat("test", 10000))
    for link in l1:
        print(link, end="\n")


test()
